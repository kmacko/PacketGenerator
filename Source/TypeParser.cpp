#include "stdafx.h"
#include "TypeParser.h"

#define ERR_CONTINUE        0
#define ERR_FAILED          1
#define ERR_BREAK           2

#define R(x)                            if (false == x) { return false; }

// wstring 에서 개행 문자 제거 매크로.
#define REMOVE_NEW_LINE(x)              size_t P(x.find(C_RET));\
                                        if (x.npos != P)\
                                        {\
                                            x.erase(x.begin() + P);\
                                        }

// 줄 처음의 명령 체크 매크로.
#define CHECK_COMMAND(p1, p2, p3, ret)  if (p1 == p2)\
                                        {\
                                            _vecParseState.push_back(p3);\
                                            return ret;\
                                        }


TypeParser::TypeParser()
    : _pFile(nullptr),
      _pParsedData(nullptr)
{
}

TypeParser::~TypeParser()
{
}

bool TypeParser::Parse(const wchar_t* pFile)
{
    if (0 != _wfopen_s(&_pFile, pFile, L"r"))
    {
        PRINT(L"파일열기 실패! " << pFile << endl);
        return false;
    }

    _PacketFileName = pFile;
    g_RemoveStr(_PacketFileName, PACKET_FILE_EXT, true);

    _vecParseState.push_back(kSearchingCommand);
    _pParsedData = new TotalParsedData();

    do
    {
        fgetws(_Buffer, LINE_BUFF_SIZE, _pFile);

        // 범위 주석을 제거하는중에 파일 끝을 만나면 에러.
        if (kRemoveRangeComment == CurParseState() && 0 != feof(_pFile))
        {
            PRINT(L"/* 주석 범위 끝인 */ 선언이 없습니다!" << endl);
            return false;
        }

        if (false == ParseLine())
        {
            fclose(_pFile);
            return false;
        }
    }
    while (0 == feof(_pFile));

    fclose(_pFile);

    return true;
}

void TypeParser::Release()
{
    SAFE_DELETE(_pParsedData);
    SAFE_DELETE_MAP(_mapIncludes);
}

wstring& TypeParser::GetPacketFileName()
{
    return _PacketFileName;
}

TotalParsedData* TypeParser::GetParsedData()
{
    return _pParsedData;
}

MAP_Includes& TypeParser::GetIncludesData()
{
    return _mapIncludes;
}

bool TypeParser::ParseLine()
{
    vector<wstring> LineWords;
    TOKEN(_Buffer, L' ', LineWords);

    // 단어 목록이 하나 이상이고 개행문자열 하나만 있는게 아니면
    if (1 <= LineWords.size() && S_RET != LineWords[0])
    {
        // 단어 목록 마지막에 개행 문자열을 따로 넣는다.
        LineWords.push_back(wstring(S_RET));
    }

    for (wstring Word : LineWords)
    {
        auto State(CurParseState());

        // 빈문자열은 스킵.
        if (0 == Word.size())
        {
            continue;
        }
        // 문장 자체가 엔터 하나만 있는 경우가 아니면 단어에서 개행문자를 제거한다.
        if (S_RET != Word)
        {
            REMOVE_NEW_LINE(Word);
        }

        switch (State)
        {
        case kSearchingCommand:         { R(TryReadCommand(Word)); }            break;
        case kReadEnums:                { R(ReadEnums(Word)); }                 break;
        case kReadEnumsOpen:            { R(ReadEnumsOpen(Word)); }             break;
        case kReadEnumsData:            { R(ReadEnumsData(Word)); }             break;
        case kReadEnumsDataAfter:       { R(ReadEnumsDataAfter(Word)); }        break;
        case kReadEnumsDataValue:       { R(ReadEnumsDataValue(Word)); }        break;
        case kReadStruct:               { R(ReadStruct(Word)); }                break;
        case kReadStructOpen:           { R(ReadStructOpen(Word)); }            break;
        case kReadStructMemberType:     { R(ReadStructMemberType(Word)); }      break;
        case kReadStructMember:         { R(ReadStructMember(Word)); }          break;
        case kRemoveRangeComment:       { R(RemoveRangedComment(Word)); }       break;
        case kReadComment:              { R(ReadComment(Word)); }               break;
        case kReadNamespace:            { R(ReadNamespace(Word)); }             break;
        case kReadIncludePacket:        { R(ReadIncludePacket(Word)); }         break;
        case kReadEncryptBitCodeStr:    { R(ReadBitCodeStr(Word)); }            break;
        default:
            {
                PRINT(L"알수 없는 파서 상태값 내부 오류! _State:" << CurParseState() << endl);
                return false;
            }
        }
    }

    return true;
}

bool TypeParser::TryReadCommand(wstring& Word)
{
    // 주석.
    int Err(CheckCommentsCommand(Word));

    if(ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK == Err)
    {
        return true;
    }

    // 네임스페이스.
    CHECK_COMMAND(TYPE_NAMESPACE, Word, kReadNamespace, true);

    // 열거자.
    CHECK_COMMAND(TYPE_ENUM, Word, kReadEnums, true);

    // 구조체 선언.
    CHECK_COMMAND(TYPE_STRUCT, Word, kReadStruct, true);

    // 다른 패킷 포함.
    CHECK_COMMAND(TYPE_INCLUDE, Word, kReadIncludePacket, true);

    // 암호화시 사용할 비트 코드값.
    CHECK_COMMAND(TYPE_ENCRYPT_BIT, Word, kReadEncryptBitCodeStr, true);

    // 단순 개행 문자열 이면 무시.
    if (S_RET == Word)
    {
        return true;
    }

    // 명령도 아니고 주석도 아닌 것이면 알 수 없는 문자열 에러
    PRINT(L"확인 되지 않은 커맨드 문자열 입니다! \"" << Word << L"\"" << endl);
    return false;
}

bool TypeParser::RemoveRangedComment(wstring& Word)
{
    // 주석 종료 문자열 이면 파싱 상태 이전으로 복구.
    if (TYPE_RANGE_COMMENT_END == Word)
    {
        PopState();
    }

    return true;
}

bool TypeParser::ReadComment(wstring& Word)
{
    // 개행 문자가 포함되어 있으면 주석 읽기모드를 빠져나간다.
    bool IsEndLineWord((Word.npos != Word.find(C_RET)) ? true : false);

    if (true == IsEndLineWord)
    {
        PopState();
    }

    return true;
}

bool TypeParser::ReadNamespace(wstring& Word)
{
    _pParsedData->AddNamespaceData(Word);
    PopState();
    return true;
}

bool TypeParser::ReadEnums(wstring& Word)
{
    if (false == CheckTypeValueName(Word))
    {
        PRINT(L"잘못된 열거자 이름입니다. \"" << Word << "\"" << endl);
        return false;
    }

    // 다음 열거자 선언({) 스코프를 찾는다.
    PopState();
    _vecParseState.push_back(kReadEnumsOpen);

    _pParsedData->AddEnumName(Word);

    return true;
}

bool TypeParser::ReadEnumsOpen(wstring& Word)
{
    // 열거자 이름 후에 들어올 수도 있는 것. 주석 패턴.
    int Err(CheckCommentsCommand(Word));

    if(ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK== Err)
    {
        return true;
    }

    if (TYPE_BRACE_OPEN == Word)
    {
        PopState();
        _vecParseState.push_back(kReadEnumsData);
        return true;
    }

    if (TYPE_BRACE_CLOSE == Word)
    {
        PRINT(L"열거자 선언 {}가 맞지 않습니다. \"" << Word << "\"" << endl);
        return false;
    }

    if (S_RET == Word)
    {
        return true;
    }

    PRINT(L"열거자 선언 오픈을({) 읽는중 알 수 없는 명령어. \"" << Word << "\"" << endl);
    return false;
}

bool TypeParser::ReadEnumsData(wstring& Word)
{
    // 열거자 이름 읽는 중 들어올 수도 있는 것. 주석 패턴.
    int Err(CheckCommentsCommand(Word));

    if (ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK == Err)
    {
        return true;
    }

    // 단순 개행 문자열 이면 무시.
    if (S_RET == Word)
    {
        return true;
    }

    // 열거자 선언 종료.
    if (TYPE_BRACE_CLOSE == Word)
    {
        PopState();
        return true;
    }

    if (false == CheckTypeValueName(Word))
    {
        PRINT(L"잘못된 열거자 값 이름 입니다." << Word << endl);
        return false;
    }

    _NameCached = Word;     // 이름으로 캐시 한다.
    _vecParseState.push_back(kReadEnumsDataAfter);

    return true;
}

bool TypeParser::ReadEnumsDataAfter(wstring& Word)
{
    int Err(CheckCommentsCommand(Word));

    bool InstertData(false);

    // 주석으로 인식됨.
    if (ERR_BREAK == Err)
    {
        InstertData = true;
    }
    else if (ERR_FAILED == Err)
    {
        return false;
    }

    // 단순 개행 문자이면 열거자 등록
    if (S_RET == Word)
    {
        InstertData = true;
    }

    if (true == InstertData)
    {
        // 자동 값 매김 처리로 열거자 이름 등록.
        _pParsedData->AddEnumData(_NameCached);

        PopState();     // 열거자 값 읽기 상태로 돌아감.
        return true;
    }

    // 대입 문자열.
    if (VLAUE_INPUT == Word)
    {
        PopState();
        _vecParseState.push_back(kReadEnumsDataValue);
        return true;
    }

    PRINT(L"열거자 값 선언 후 알수 없는 커맨드." << Word << endl);
    return false;
}

bool TypeParser::ReadEnumsDataValue(wstring& Word)
{
    int Err(CheckCommentsCommand(Word));

    // 주석으로 인식됨.
    if (ERR_BREAK == Err)
    {
        return true;
    }
    else if (ERR_FAILED == Err)
    {
        return false;
    }

    if (false == CheckNumberString(Word, false))
    {
        PRINT(L"열거자의 값은 정수 입니다. :" << Word << endl);
        return false;
    }

    _pParsedData->AddEnumData(_NameCached, _wtoi(Word.c_str()));
    PopState();     // 열거자 값 읽기로 돌아간다.

    return true;
}

bool TypeParser::ReadStruct(wstring& Word)
{
    if (false == CheckTypeValueName(Word))
    {
        PRINT(L"잘못된 구조체 이름입니다. \"" << Word << "\"" << endl);
        return false;
    }

    // 다음 열거자 선언({) 스코프를 찾는다.
    PopState();
    _vecParseState.push_back(kReadStructOpen);

    _pParsedData->AddNewStruct(Word);

    return true;
}

bool TypeParser::ReadStructOpen(wstring& Word)
{
    // 구조체 선언 후에 들어올 수도 있는 것. 주석 패턴.
    int Err(CheckCommentsCommand(Word));

    if (ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK == Err)
    {
        return true;
    }

    if (true == g_ContainsStr(Word, TYPE_ENCRYPT, true))
    {
        _pParsedData->SetStructEncrypt();
        return true;
    }

    if (TYPE_BRACE_OPEN == Word)
    {
        PopState();
        // 멤버 읽기로 넘어간다.
        _vecParseState.push_back(kReadStructMemberType);
        return true;
    }

    if (TYPE_BRACE_CLOSE == Word)
    {
        PRINT(L"구조체 선언 {}가 맞지 않습니다. \"" << Word << "\"" << endl);
        return false;
    }

    if (S_RET == Word)
    {
        return true;
    }

    PRINT(L"구조체 선언 오픈을({) 읽는중 알 수 없는 명령어. \"" << Word << "\"" << endl);
    return false;
}

bool TypeParser::ReadStructMemberType(wstring& Word)
{
    // 구조체 멤버 읽는 중 들어올 수도 있는 것. 주석 패턴.
    int Err(CheckCommentsCommand(Word));

    if (ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK == Err)
    {
        return true;
    }

    if (TYPE_BRACE_CLOSE == Word)
    {
        PopState();         // 이전 상태로 돌아간다. (명령 읽기)
        return true;
    }

    if (S_RET == Word)
    {
        return true;
    }

    // 컨테이너 선언인 경우.
    wstring Temp(Word);
    if (true == GetContainerName(Temp, _ContainerTypeName))
    {
        g_RemoveStr(Temp, TYPE_LIST_OPEN, true);

        // list< 삭제 성공 한 경우. 나머지 '>' 문자도 제거. 요게 컨테이너 자료형이 된다.
        g_RemoveStr(Temp, TYPE_LIST_CLOSE);
    }

    wstring TypeName(Temp);
    if (false == CheckTypeName(Temp, TypeName))
    {
        PRINT(L"잘못된 자료형 이름 입니다." << Temp << endl);
        return false;
    }

    _NameCached = TypeName;                         // 자료형 이름 캐시 한다.
    _vecParseState.push_back(kReadStructMember);    // 변수명 읽기로 넘김.

    return true;
}

bool TypeParser::ReadStructMember(wstring& Word)
{
    int Err(CheckCommentsCommand(Word));
    if(ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK == Err)
    {
        PRINT(L"구조체 멤버 자료형 멤버이름 사이에 주석을 넣지 마세요. \"" << _NameCached << "\"" << " \"" << Word << "\"" << endl);
        return false;
    }

    // 단순 개행 문자열 이면 무시.
    if (S_RET == Word)
    {
        return true;
    }

    int ArrayCnt = 1;
    // 배열 선언 체크.
    if (true == g_ContainsStr(Word, ARRAY_OPEN))
    {
        if (false == g_ContainsStr(Word, ARRAY_CLOSE))
        {
            PRINT(L"구조체 멤버 배열 선언이 이상합니다. \"" << Word << "\"" << endl);
            return false;
        }

        wstring Temp(Word);
        // 숫자만 찾는다.
        size_t ArrayOpenPos(Temp.find(ARRAY_OPEN));
        Temp.erase(Temp.begin(), Temp.begin() + ArrayOpenPos + 1);

        size_t ArrayClosePos(Temp.find(ARRAY_CLOSE));
        Temp.erase(Temp.begin() + ArrayClosePos, Temp.begin() + ArrayClosePos + wcslen(ARRAY_CLOSE));

        if (false == CheckNumberString(Temp, false))
        {
            PRINT(L"구조체 멤버 배열 선언이 이상합니다. \"" << Word << "\"" << endl);
            return false;
        }

        ArrayCnt = _wtoi(Temp.c_str());

        // Word를 배열을 제외한 변수명으로 바꾼다.
        Word.erase(Word.begin() + ArrayOpenPos, Word.end());
    }

    if (false == CheckTypeValueName(Word))
    {
        PRINT(L"선언 불가능한 멤버 이름 입니다. \"" << Word << "\"" << endl);
        return false;
    }

    _pParsedData->AddStructMember(_NameCached, Word, ArrayCnt, _ContainerTypeName);

    PopState();

    return true;
}

bool TypeParser::ReadIncludePacket(wstring& Word)
{
    int Err(CheckCommentsCommand(Word));
    if (ERR_FAILED == Err)
    {
        return false;
    }
    else if (ERR_BREAK == Err)
    {
        PRINT(L"include 파일명 사이에 주석을 넣지 마세요. \"" << Word << "\"" << endl);
        return false;
    }

    // 문자열 쌍따옴표 있는지 체크.
    if (false == g_ContainsStr(Word, L"\""))
    {
        PRINT(L"include 예시) include \"파일명\". --- \"" << Word << "\"" << endl);
        return false;
    }

    // 두번 호출. 앞뒤.
    g_RemoveStr(Word, L"\"");
    g_RemoveStr(Word, L"\"");

    // 대상 패킷 파일 경로 작성.
    wchar_t Path[MAX_PATH] = { 0 };
    GetCurrentDirectoryW(MAX_PATH, Path);

    wstring wPath(Path);
    wchar_t LastChar(wPath[wPath.size() - 1]);

    // 마지막에 디렉토리 정보가 없으면 추가.
    if (L'\\' != LastChar || L'/' != LastChar)
    {
        wPath += L'\\';
    }

    wPath += Word;

    TypeParser IncludeParser;
    if (false == IncludeParser.Parse(wPath.c_str()))
    {
        PRINT(L"include 파싱 실패 했습니다. - " << wPath << endl);
        return false;
    }

    // 확장자는 제거 한다.
    wstring IncludeName(Word);
    size_t ExtPos(IncludeName.find_last_of(L"."));
    if (IncludeName.npos != ExtPos)
    {
        IncludeName.erase(IncludeName.begin() + ExtPos, IncludeName.end());
    }

    // 파싱 한것들을 Include 목록에 넣는다.
    _mapIncludes.insert(MAP_IncludesVal(IncludeName, IncludeParser._pParsedData));
    // Include 에서도 조회 한다.
    for (auto& v : IncludeParser._mapIncludes)
    {
        auto f(_mapIncludes.find(v.first));

        if (f != _mapIncludes.end())
        {
            PRINT(L"경고 include 중복이 발견 되었습니다. " << v.first << endl);
        }

        _mapIncludes.insert(v);
    }

    PopState();

    return true;
}

bool TypeParser::ReadBitCodeStr(wstring& Word)
{
    _pParsedData->_EncryptBitCode = Word;

    PopState();

    return true;
}

TypeParser::eParseState TypeParser::CurParseState()
{
    if (0 >= _vecParseState.size())
    {
        return kError;
    }

    return _vecParseState[_vecParseState.size() - 1];
}

void TypeParser::PopState()
{
    if (0 >= _vecParseState.size())
    {
        PRINT(L"내부 오류. 상태가 없는데 PopState가 발생했습니다." << endl);
        return;
    }

    _vecParseState.erase(_vecParseState.begin() + _vecParseState.size() - 1);
}

bool TypeParser::GetContainerName(wstring& Word, wstring& RetContainerName)
{
    RetContainerName = wstring();
    wstring ContainerName;
    // Include 형 타입이 아니면. 컨테이너 선언 인지 조사.
    if (true == g_ContainsStr(Word, TYPE_LIST_OPEN, true))
    {
        RetContainerName = TYPE_LIST;
        return true;
    }
    return false;
}

bool TypeParser::CheckTypeName(wstring& Word, wstring& RetTypeName)
{
    wstring IncludeName;

    size_t Pos(Word.find(L'.'));
    if (Word.npos != Pos)
    {
        IncludeName = wstring(Word.begin(), Word.begin() + Pos);
        RetTypeName = wstring(Word.begin() + Pos + 1, Word.end());
    }
    else
    {
        IncludeName = wstring();
        RetTypeName = Word;
    }

    // 인클루드의 자료형 호출하는 경우 Include.자료형 형태로 사용 할 수 있음.
    if (false == CheckTypeValueName(RetTypeName))
    {
        return false;
    }

    // 인클루드 이름을 사용 시도 한것이면 체크.
    if (0 != IncludeName.size())
    {
        // 추후 
        const auto& Found(_mapIncludes.find(IncludeName));

        if (_mapIncludes.end() == Found)
        {
            PRINT(L"존재하지 않는 Include 패킷파일명 입니다. \"" << IncludeName << "\"" << endl);
            return false;
        }

        if (false == Found->second->IsExistStructName(RetTypeName))
        {
            PRINT(L"확인 할 수 없는 자료형 입니다. \"" << Word << "\"" << endl);
            return false;
        }
    }

    return true;
}

bool TypeParser::CheckTypeValueName(wstring& Word)
{
    if (S_RET == Word || 0 >= Word.size())
    {
        return false;
    }

    wstring Clone(Word);
    TOLOWER(Clone);     // 체크를 위해 소문자로 만듬.

    for (size_t i(0) ; Clone.size() > i ; ++i)
    {
        wchar_t wc(Clone[i]);
        bool IsChar(true);
        bool IsNumber(false);

        // '_' 및 알파벳이면 OK.
        if (L'_' != wc &&
            (L'a' > wc || L'z' < wc))
        {
            IsChar = false;
        }

        if (L'0' <= wc && L'9' >= wc)
        {
            IsNumber = true;
        }

        // 첫글자 이고 숫자면 불가.
        if (0 == i && true == IsNumber)
        {
            return false;
        }

        // 첫글자가 아니고 숫자, 알파벳 둘다 아니면 불가.
        if (0 != i && false == IsChar && false == IsNumber)
        {
            return false;
        }
    }

    return true;
}

bool TypeParser::CheckNumberString(wstring& Word, bool IsFloat)
{
    wstring NumStr(Word);
    size_t Count(NumStr.size());

    for (wchar_t wc : NumStr)
    {
        bool IsOK(false);
        // 숫자면 OK
        if (L'0' <= wc && L'9' >= wc)
        {
            IsOK = true;
        }

        // 안되는거 중에 실수면 '.' 인경우 OK
        if (false == IsOK && true == IsFloat && L'.' == wc)
        {
            IsOK = true;
        }

        if (false == IsOK)
        {
            return false;
        }
    }

    return true;
}

int TypeParser::CheckCommentsCommand(wstring& Word)
{
    CHECK_COMMAND(TYPE_RANGE_COMMENT_BEGIN, Word, kRemoveRangeComment, ERR_BREAK);

    if (TYPE_RANGE_COMMENT_END == Word)
    {
        PRINT(L"'/*' 주석 선언 없는 종료 주석 발생!" << endl);
        return ERR_FAILED;
    }

    // 주석 데이터. // 를 //// 이렇게도 넣기 때문에 검색으로 확인한다.
    if (Word.npos != Word.find(TYPE_COMMENT))
    {
        _vecParseState.push_back(kReadComment);
        return ERR_BREAK;
    }

    return ERR_CONTINUE;
}
