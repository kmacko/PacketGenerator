#pragma once

class StructData;

class CppGenerator
{
public :
    CppGenerator();
    ~CppGenerator();

public :
    void                        Initialize(const wchar_t* pOutPath, TypeParser* pParser);
    bool                        GenerateSource();

private :
    wstring                     GetOutputPath(const wchar_t* pOutPath);
    bool                        OpenFiles(const wchar_t* pOutPath);
    void                        FileClose();

    void                        GenerateHeader();
    void                        GenerateEnumsToHeader();
    void                        GenerateStructToHeader();

    void                        GenerateCpp();
    void                        GenerateEnumsToCpp();
    void                        GenerateStructToCpp();
    void                        GenerateStructToCppOperatorFunc(StructData* pData);
    void                        GenerateStructToCppConstructWithMembers(StructData* pData);
    void                        GenerateStructToCppSetData(StructData* pData);
    void                        GenerateStructToCppSetPacket(StructData* pData);

    wstring                     GetTypeName(wstring& Container, wstring& Type);
    wstring                     GetTypeName(wstring& Type);
    wstring                     GetMemberName(wstring& Name, int ArrayCnt);
    wstring                     GetConstructMemberListStr(StructData* pData, vector<wstring>& FinalTypes);
    bool                        IsEnumTypeName(wstring& Type);
    bool                        IsDefaultTypeName(wstring& Type);
    bool                        IsProtoalStructName(wstring& StructName);
    wstring                     GetProtocolNameFromMessageTypeEnum(wstring& EnumData);

    void                        WriteHeader(const wstring& Str);
    void                        WriteCpp(const wstring& Str);

private :
    TotalParsedData*            _pParsedData;
    MAP_Includes                _mapIncludes;

    FILE*                       _pHeaderFile;
    FILE*                       _pCppFile;

    wstring                     _OutFilePath;
    wstring                     _PacketFileName;
    wstring                     _PacketFileNameNoExt;
};

