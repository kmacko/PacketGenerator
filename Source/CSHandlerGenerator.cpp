#include "stdafx.h"
#include "CSDefines.h"
#include "CS_SRC_Packet.h"
#include "CS_SRC_Handler.h"
#include "TypeParser.h"
#include "CSHandlerGenerator.h"

typedef map<wstring, FILE*>             MAP_FilePtr;
typedef MAP_FilePtr::iterator           MAP_FilePtrIter;
typedef MAP_FilePtr::value_type         MAP_FilePtrVal;

CSHandlerGenerator::CSHandlerGenerator()
    : _pFPBase(nullptr)
{
}

CSHandlerGenerator::~CSHandlerGenerator()
{
}

bool CSHandlerGenerator::Initialize(wstring& OutPath, vector<TypeParser*>& vecParser)
{
    _OutPath = OutPath;

    wchar_t LastChar(*_OutPath.rbegin());

    // 마지막 글자가 \\, / 가 아니면 \\ 붙인다.
    if (L'\\' != LastChar || L'/' != LastChar)
    {
        _OutPath += L'\\';
    }

    _vecParsers = vecParser;        // 복사됨.

    return true;
}

bool CSHandlerGenerator::Generate()
{
    // 생성하기 전에 모든 열거자 목록의 구조체 선언이 있는지 체크 한다.
    if (false == CheckProtocols())
    {
        return false;
    }

    if (false == OpenFiles())
    {
        CloseFiles();
        return false;
    }

    _vecUsedNamespaces.clear();
    if (false == GenerateBaseCode())
    {
        CloseFiles();
        return false;
    }

    for (auto* pParser : _vecParsers)
    {
        // 파싱된 것에 프로토콜 이름과 동일한 구조체가 없으면 해당 Partial cs 파일 생성안함.
        if (false == HasProtocolStructName(pParser->GetParsedData()))
        {
            continue;
        }

        _vecUsedNamespaces.clear();
        if (false == GenerateBasePartialCode(pParser))
        {
            CloseFiles();
            return false;
        }
    }

    CloseFiles();
    return true;
}

bool CSHandlerGenerator::CheckProtocols()
{
    auto* pMessageEnum(GetMessageTypeData());
    if (nullptr == pMessageEnum)
    {
        PRINT(PACKET_MESSAGE_TYPE_NAME << L"이름의 열거자가 없습니다. 프로토콜 정의를 확인 할 수 없습니다!" << endl);
        return false;
    }

    for (wstring EnumDataName : pMessageEnum->_EnumDatas)
    {
        wstring ProtocolName(GetProtocolNameFromMessageTypeEnum(EnumDataName));

        // 모든 구조체 조회 한다. 여기에 인클루드는 포함되지 않는다.
        bool bFound(false);
        for (auto* pParser : _vecParsers)
        {
            auto* TotData(pParser->GetParsedData());

            for (auto& Struct : TotData->_Structs)
            {
                if (ProtocolName == Struct._StructName)
                {
                    bFound = true;
                    break;
                }
            }

            // 찾았으면 루프 중단.
            if (true == bFound)         { break;}
        }

        // 못찾은 경우 에러는 낸다.
        if (false == bFound)
        {
            PRINT(L"!!!경고!!! 열거자데이터 : " << EnumDataName << L" 프로토콜 이름(" << ProtocolName << L") 매칭되지 않았습니다." << endl);
            return false;
        }
    }

    return true;
}

bool CSHandlerGenerator::OpenFiles()
{
    wstring BaseFile(_OutPath + OUT_HANDLER_FILE_NAME);

    auto* pBaseFile(WC2C(BaseFile.c_str()));
    if (0 != fopen_s(&_pFPBase, pBaseFile, "w+"))
    {
        PRINT(BaseFile << L" 파일 쓰기모드 열기 실패!" << endl);
        return false;
    }

    for (auto* pParser : _vecParsers)
    {
        // 파싱된 것에 프로토콜 이름과 동일한 구조체가 없으면 해당 Partial cs 파일 생성안함.
        if (false == HasProtocolStructName(pParser->GetParsedData()))
        {
            continue;
        }

        wstring PartialName(OUT_HANDLER_FILE_PARTIAL_NAME);
        REPLACE(PartialName, CS_IDF_P1, pParser->GetPacketFileName());

        wstring FilePath(_OutPath + PartialName);

        auto* pFile(WC2C(FilePath.c_str()));
        FILE* pFP(nullptr);

        if (0 != fopen_s(&pFP, pBaseFile, "w+"))
        {
            PRINT(FilePath << L" 파일 쓰기모드 열기 실패!" << endl);
            return false;
        }

        _mapFPPartials.insert(MAP_FilePtrVal(pParser->GetPacketFileName(), pFP));
    }

    return true;
}

void CSHandlerGenerator::CloseFiles()
{
    if (nullptr != _pFPBase)
    {
        fclose(_pFPBase);
        _pFPBase = nullptr;
    }

    for (auto& Iter : _mapFPPartials)
    {
        fclose(Iter.second);
    }
    _mapFPPartials.clear();
}


void CSHandlerGenerator::WriteBase(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pFPBase);
}

void CSHandlerGenerator::WritePartial(const wstring& PartialName, const wstring& Str)
{
    if (_mapFPPartials.end() == _mapFPPartials.find(PartialName))
    {
        PRINT(L"내부 오류. 매칭되는 이름의 파일이 열리지 않았습니다. PrtialName:" << PartialName << endl);
        return;
    }

    FILE* pFP(_mapFPPartials[PartialName]);

    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), pFP);
}

bool CSHandlerGenerator::GenerateBaseCode()
{
    wstring CurrTime(GET_TIME());

    wstring CommentStr(CS_HandlBaseCommment);
    REPLACE(CommentStr, CS_IDF_P1, CurrTime);
    WriteBase(CommentStr);

    for (auto* pParser : _vecParsers)
    {
        auto vecNames(MakeNameSpaceList(pParser->GetParsedData()));
        WriteUseingNamespace(vecNames);
    }

    // OnMessage, SetCallBack 함수의 스위치문장의 패턴이 같아서 한루프에서 같이 수집.
    vector<wstring> vecMessageCase;
    vector<wstring> vecSetCallBackCase;
    auto* pEnum(GetMessageTypeData());
    for (wstring& EnumDataName : pEnum->_EnumDatas)
    {
        wstring EnumName(pEnum->_Name);
        wstring Protocol(GetProtocolNameFromMessageTypeEnum(EnumDataName));

        wstring CaseMessage(CS_HandlBaseOnMessageCaseEnum);
        REPLACE(CaseMessage, CS_IDF_P1, EnumName);
        REPLACE(CaseMessage, CS_IDF_P2, EnumDataName);
        REPLACE(CaseMessage, CS_IDF_P3, Protocol);
        vecMessageCase.push_back(CaseMessage);

        wstring CaseSetCallBack(CS_HandlBaseSetCallBackCaseEnum);
        REPLACE(CaseSetCallBack, CS_IDF_P1, EnumName);
        REPLACE(CaseSetCallBack, CS_IDF_P2, EnumDataName);
        REPLACE(CaseSetCallBack, CS_IDF_P3, Protocol);
        vecSetCallBackCase.push_back(CaseSetCallBack);
    }

    WriteBase(CS_HandlBaseClassOpen);
    for (wstring& Case : vecMessageCase)
    {
        WriteBase(Case);
    }
    WriteBase(CS_HandlBaseOnMessageClose);

    WriteBase(CS_HandlBaseSetCallBackOpen);
    for (wstring& Case : vecSetCallBackCase)
    {
        WriteBase(Case);
    }
    WriteBase(CS_HandlBaseSetCallBackClose);

    // 암호화.
    wstring EncryptOpen(CS_HandlBaseEncrypt_Open);
    REPLACE(EncryptOpen, CS_IDF_P1, PACKET_MESSAGE_TYPE_NAME);
    WriteBase(EncryptOpen);

    for (auto* pParser : _vecParsers)
    {
        for (auto& Data : pParser->GetParsedData()->_Structs)
        {
            if (true == Data._IsEncrypt)
            {
                wstring CaseStr(CS_HandlBaseEncryptDecrypt_Case);
                REPLACE(CaseStr, CS_IDF_P1, PACKET_MESSAGE_TYPE_NAME);
                REPLACE(CaseStr, CS_IDF_P2, Data._StructName);
                WriteBase(CaseStr);
            }
        }
    }

    wstring EncryptClose(CS_HandlBaseEncryptDecrypt_Close);
    wstring EncryptBitCodeStr(GetEncryptBitCodeStr());
    if (0 >= EncryptBitCodeStr.size())
    {
        PRINT(L"EncryptBitCode 값이 설정 되지 않았습니다." << endl);
        return false;
    }
    REPLACE(EncryptClose, CS_IDF_P1, EncryptBitCodeStr);
    WriteBase(EncryptClose);

    // 복호화.
    wstring DecryptOpen(CS_HandlBaseDecrypt_Open);
    REPLACE(DecryptOpen, CS_IDF_P1, PACKET_MESSAGE_TYPE_NAME);
    WriteBase(DecryptOpen);

    for (auto* pParser : _vecParsers)
    {
        for (auto& Data : pParser->GetParsedData()->_Structs)
        {
            if (true == Data._IsEncrypt)
            {
                wstring CaseStr(CS_HandlBaseEncryptDecrypt_Case);
                REPLACE(CaseStr, CS_IDF_P1, PACKET_MESSAGE_TYPE_NAME);
                REPLACE(CaseStr, CS_IDF_P2, Data._StructName);
                WriteBase(CaseStr);
            }
        }
    }

    wstring DecryptClose(CS_HandlBaseEncryptDecrypt_Close);
    REPLACE(DecryptClose, CS_IDF_P1, EncryptBitCodeStr);
    WriteBase(DecryptClose);

    wstring EndStr(CS_PacketPartialClassEnd);
    REPLACE(EndStr, CS_IDF_P1, OUT_HANDLER_FILE_NAME);
    WriteBase(EndStr);

    return true;
}

bool CSHandlerGenerator::GenerateBasePartialCode(TypeParser* pParser)
{
    TotalParsedData* pData(pParser->GetParsedData());
    wstring CurrTime(GET_TIME());
    wstring PN(pParser->GetPacketFileName());   // PartialName 패킷 정의 파일 이름.

    wstring CommentStr(CS_HandlPartialComment);
    REPLACE(CommentStr, CS_IDF_P1, pParser->GetPacketFileName());
    REPLACE(CommentStr, CS_IDF_P2, CurrTime);
    WritePartial(PN, CommentStr);

    for (auto* pParser : _vecParsers)
    {
        auto vecNames(MakeNameSpaceList(pParser->GetParsedData()));
        WriteUseingNamespace(vecNames, PN);
    }
    
    WritePartial(PN, CS_HandlPartialClassOpen);

    bool IsFirstOut(true);
    // 구조체 목록 기준으로 패킷을 출력.
    for (auto& Data : pData->_Structs)
    {
        if (true == IsProtoalStructName(Data._StructName))
        {
            wstring EnumName(GetMessageTypeEnumByProtocolName(Data._StructName));
            if (0 >= EnumName.size())
            {
                PRINT(Data._StructName << L"과 일치하는" << PACKET_MESSAGE_TYPE_NAME << L"내 열거자이름이 없습니다.");
                return false;
            }

            wstring PacketStr(CS_HandlPartialPacketDeclear);
            REPLACE(PacketStr, CS_IDF_P1, Data._StructName);
            REPLACE(PacketStr, CS_IDF_P2, PACKET_MESSAGE_TYPE_NAME);
            REPLACE(PacketStr, CS_IDF_P3, EnumName);

            // 처음게 아니면 출력 전에 엔터.
            if (false == IsFirstOut)
            {
                WritePartial(PN, wstring(L"\n"));
            }

            WritePartial(PN, PacketStr);
            IsFirstOut = false;
        }
    }

    wstring PartialName(OUT_HANDLER_FILE_PARTIAL_NAME);
    REPLACE(PartialName, CS_IDF_P1, PN);

    wstring EndStr(CS_PacketPartialClassEnd);
    REPLACE(EndStr, CS_IDF_P1, PartialName);
    WritePartial(PN, EndStr);

    return true;
}

void CSHandlerGenerator::WriteUseingNamespace(vector<wstring>& NamespaceList, wstring PacketFileName)
{
    for (wstring& OutName : NamespaceList)
    {
        bool IsUsed(false);

        // 이미 사용된 네임스페이스 인지 체크.
        for (wstring& Check : _vecUsedNamespaces)
        {
            if (OutName == Check)
            {
                IsUsed = true;
                break;
            }
        }

        if (false == IsUsed)
        {
            if (0 >= PacketFileName.size())
            {
                WriteBase(OutName);
            }
            else
            {
                WritePartial(PacketFileName, OutName);
            }

            // 사용된 네임스페이스 목록에 추가.
            _vecUsedNamespaces.push_back(OutName);
        }
    }
}

vector<wstring> CSHandlerGenerator::MakeNameSpaceList(TotalParsedData* pData)
{
    vector<wstring> Namespaces;

    wstring PrevNamespace;
    for (size_t i(0) ; pData->_NamespaceData.size() > i ; ++i)
    {
        wstring Name(pData->_NamespaceData[i]);

        if (0 >= PrevNamespace.size())
        {
            PrevNamespace = Name;
        }
        else
        {
            wstring Format(CS_HandlBaseNamespaceAddFormat);
            REPLACE(Format, CS_IDF_P1, PrevNamespace);
            REPLACE(Format, CS_IDF_P2, Name);
            PrevNamespace = Format;
        }

        wstring NameSpaceStr(CS_HandlBaseUseNamespace);
        REPLACE(NameSpaceStr, CS_IDF_P3, PrevNamespace);
        Namespaces.push_back(NameSpaceStr);
    }

    return Namespaces;
}

EnumData* CSHandlerGenerator::GetMessageTypeData()
{
    // 파싱된 것중에 MessageType 열거자 선언을 찾는다.
    for (auto* pParser : _vecParsers)
    {
        for (auto& Data : pParser->GetParsedData()->_Enums)
        {
            if (PACKET_MESSAGE_TYPE_NAME == Data._Name)
            {
                return &Data;
            }
        }
    }

    return nullptr;
}

wstring CSHandlerGenerator::GetEncryptBitCodeStr()
{
    for (auto* pParser : _vecParsers)
    {
        if(0 < pParser->GetParsedData()->_EncryptBitCode.size())
        {
            return pParser->GetParsedData()->_EncryptBitCode;
        }
    }
    return L"";
}

wstring CSHandlerGenerator::GetProtocolNameFromMessageTypeEnum(wstring& EnumData)
{
    if (0 >= EnumData.size()) { return wstring(); }

    wchar_t FirstCh(EnumData[0]);

    // 소문자 글자 이면 첫글자를 지운다.
    if (L'a' <= FirstCh && L'z' >= FirstCh)
    {
        wstring Clone(EnumData.begin() + 1, EnumData.end());
        return Clone;
    }
    // 아니면 그대로 리턴.
    return EnumData;
}

wstring CSHandlerGenerator::GetMessageTypeEnumByProtocolName(wstring& StructName)
{
    auto* pEnumData(GetMessageTypeData());

    for (wstring& EnumDataName : pEnumData->_EnumDatas)
    {
        wstring ProtocolName(GetProtocolNameFromMessageTypeEnum(EnumDataName));

        if (ProtocolName == StructName)
        {
            return EnumDataName;
        }
    }

    return wstring();
}

bool CSHandlerGenerator::HasProtocolStructName(TotalParsedData* pData)
{
    for (auto& Data : pData->_Structs)
    {
        // 하나라도 있으면 참으로 리턴.
        if (true == IsProtoalStructName(Data._StructName))
        {
            return true;
        }
    }

    return false;
}

bool CSHandlerGenerator::IsProtoalStructName(wstring& StructName)
{
    auto* pEnumData(GetMessageTypeData());

    for (wstring& EnumDataName : pEnumData->_EnumDatas)
    {
        wstring ProtocolName(GetProtocolNameFromMessageTypeEnum(EnumDataName));

        if (ProtocolName == StructName)
        {
            return true;
        }
    }

    return false;
}
