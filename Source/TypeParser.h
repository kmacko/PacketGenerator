#pragma once

class TotalParsedData;

class TypeParser
{
private :
    enum eParseState
    {
        kError                      = 0,
        kSearchingCommand           = 1,        // 커맨드를 찾는중.
        kReadEnums                  = 10,       // 열거자 읽는중.
        kReadEnumsOpen              = 11,       // 열거자 열기닫기('{}') 찾는중
        kReadEnumsData              = 12,       // 열거자 멤버 읽는중.
        kReadEnumsDataAfter         = 13,       // 열거자 멤버 읽음 다음 정보 읽는중.
        kReadEnumsDataValue         = 14,       // 열거자 멤버 이름뒤에 '=' 나옴. 숫자를 받는중.
        kReadStruct                 = 20,       // 구조체 정보 읽는중.
        kReadStructOpen             = 21,       // 구조체 정의 열기 닫기 읽는중. ('{}')
        kReadStructMemberType       = 22,       // 구조체 멤버 자료형 읽는중.
        kReadStructMember           = 23,       // 구조체 멤버 변수 읽는중.
        kRemoveRangeComment         = 30,       // 주석 제거중. //는 한줄이므로 /* */ 이다.
        kReadComment                = 40,       // 한줄 주석 수집중.
        kReadNamespace              = 50,       // 네임스페이스 읽는중.
        kReadIncludePacket          = 60,       // 포함 패킷 정의 읽는중. 파서를 새로 만들고 읽게 한다.
        kReadEncryptBitCodeStr      = 70,       // 암호화 적용시 비트연산 적용할 정보를 읽는중.
    };

public :
    TypeParser();
    ~TypeParser();

public :
    bool                            Parse(const wchar_t* pFile);
    void                            Release();

    wstring&                        GetPacketFileName();
    TotalParsedData*                GetParsedData();
    map<wstring, TotalParsedData*>& GetIncludesData();

private :
    bool                            ParseLine();

    bool                            TryReadCommand(wstring& Word);
    bool                            RemoveRangedComment(wstring& Word);
    bool                            ReadComment(wstring& Word);
    bool                            ReadNamespace(wstring& Word);
    bool                            ReadEnums(wstring& Word);
    bool                            ReadEnumsOpen(wstring& Word);
    bool                            ReadEnumsData(wstring& Word);
    bool                            ReadEnumsDataAfter(wstring& Word);
    bool                            ReadEnumsDataValue(wstring& Word);
    bool                            ReadStruct(wstring& Word);
    bool                            ReadStructOpen(wstring& Word);
    bool                            ReadStructMemberType(wstring& Word);
    bool                            ReadStructMember(wstring& Word);
    bool                            ReadIncludePacket(wstring& Word);
    bool                            ReadBitCodeStr(wstring& Word);

private :
    eParseState                     CurParseState();
    void                            PopState();

    // 변수, 열거자, 구조체 이름 체크 함수.
    bool                            GetContainerName(wstring& Word, wstring& RetContainerName);
    bool                            CheckTypeName(wstring& Word, wstring& RetTypeName);
    bool                            CheckTypeValueName(wstring& Word);
    bool                            CheckNumberString(wstring& Word, bool IsFloat);
    int                             CheckCommentsCommand(wstring& Word);

private :
    wchar_t                         _Buffer[LINE_BUFF_SIZE];

    FILE*                           _pFile;
    wstring                         _PacketFileName;    // 확장자를 뺀 패킷정의 파일 이름.

    // 파싱 상태 스텍.
    vector<eParseState>             _vecParseState;

    wstring                         _NameCached;
    wstring                         _ContainerTypeName;

    vector<wstring>                 _IncludeNames;

    TotalParsedData*                _pParsedData;
    map<wstring, TotalParsedData*>  _mapIncludes;
};

