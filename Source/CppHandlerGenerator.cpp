#include "stdafx.h"
#include "CppDefines.h"
#include "CPP_SRC_H.h"
#include "CPP_SRC_CPP.h"
#include "TypeParser.h"
#include "CppHandlerGenerator.h"

CppHandlerGenerator::CppHandlerGenerator()
    : _pFPPacketHeaders(nullptr),
      _pFPHandlH(nullptr),
      _pFPHandlCpp(nullptr)
{
}

CppHandlerGenerator::~CppHandlerGenerator()
{
}

bool CppHandlerGenerator::Initialize(wstring& OutPath, vector<TypeParser*>& vecParser)
{
    _OutPath = OutPath;

    wchar_t LastChar(*_OutPath.rbegin());

    // 마지막 글자가 \\, / 가 아니면 \\ 붙인다.
    if (L'\\' != LastChar || L'/' != LastChar)
    {
        _OutPath += L'\\';
    }

    _vecParsers = vecParser;        // 복사됨.

    return true;
}

bool CppHandlerGenerator::Generate()
{
    if (false == OpenFiles())
    {
        CloseFiles();
        return false;
    }

    if (false == GeneratePacketHeadersH())
    {
        CloseFiles();
        return false;
    }

    if (false == GenerateHandlH())
    {
        CloseFiles();
        return false;
    }

    if (false == GenerateHandlCpp())
    {
        CloseFiles();
        return false;
    }

    CloseFiles();
    return true;
}

bool CppHandlerGenerator::OpenFiles()
{
    wstring PacketHeader(_OutPath + OUT_HANDL_PACKET_HEADERS);
    wstring HandlH(_OutPath + OUT_HANDL_H);
    wstring HandlCpp(_OutPath + OUT_HANDL_CPP);

    auto* pPacketHeaderFile(WC2C(PacketHeader.c_str()));
    if (0 != fopen_s(&_pFPPacketHeaders, pPacketHeaderFile, "w+"))
    {
        PRINT(PacketHeader << L" 파일 쓰기모드 열기 실패!" << endl);
        return false;
    }

    auto* pHandlH(WC2C(HandlH.c_str()));
    if (0 != fopen_s(&_pFPHandlH, pHandlH, "w+"))
    {
        PRINT(HandlH << L" 파일 쓰기모드 열기 실패!" << endl);
        return false;
    }
    auto* pHandlCpp(WC2C(HandlCpp.c_str()));
    if (0 != fopen_s(&_pFPHandlCpp, pHandlCpp, "w+"))
    {
        PRINT(HandlCpp << L" 파일 쓰기모드 열기 실패!" << endl);
        return false;
    }

    return true;
}

void CppHandlerGenerator::CloseFiles()
{
    if (nullptr != _pFPPacketHeaders)
    {
        fclose(_pFPPacketHeaders);
        _pFPPacketHeaders = nullptr;
    }
    if (nullptr != _pFPHandlH)
    {
        fclose(_pFPHandlH);
        _pFPHandlH = nullptr;
    }
    if (nullptr != _pFPHandlCpp)
    {
        fclose(_pFPHandlCpp);
        _pFPHandlCpp = nullptr;
    }
}

bool CppHandlerGenerator::GeneratePacketHeadersH()
{
    wstring CurTime(GET_TIME());

    wstring HeadComment(CPP_HandlPacketHeadersComment);
    REPLACE(HeadComment, CPP_IDF_DATE, CurTime);
    WritePacketHeaders(HeadComment);

    for (auto* pParser : _vecParsers)
    {
        wstring IncludeStr(CPP_HandlIncludePacket);
        REPLACE(IncludeStr, CPP_IDF_PACKET_FILE_NAME, pParser->GetPacketFileName());
        WritePacketHeaders(IncludeStr);
    }

    WritePacketHeaders(L"\n\n");

    _vecUsedNamespaces.clear();
    // 모든 네임스페이스를 조회 하고 using 처리.
    for (auto* pParser : _vecParsers)
    {
        auto* pData(pParser->GetParsedData());

        auto Namespaces(MakeNameSpaceList(pData));

        WriteUseingNamespace(Namespaces);
    }

    return true;
}

vector<wstring> CppHandlerGenerator::MakeNameSpaceList(TotalParsedData* pData)
{
    vector<wstring> Namespaces;

    for (size_t i(0) ; pData->_NamespaceData.size() > i ; ++i)
    {
        wstring Name(pData->_NamespaceData[i]);
        wstring PrevNameSpaceStr((0 < Namespaces.size()) ? *Namespaces.rbegin() + L"::" : L"");

        wstring NameSpaceStr(CPP_MakeNameSpceFormat);
        REPLACE(NameSpaceStr, CPP_IDF_NAMESPACE_FULL, PrevNameSpaceStr);
        REPLACE(NameSpaceStr, CPP_IDF_NAMESPACE, Name);
        Namespaces.push_back(NameSpaceStr);
    }

    return Namespaces;
}

void CppHandlerGenerator::WriteUseingNamespace(vector<wstring>& NamespaceList)
{
    for(wstring& OutName : NamespaceList)
    {
        bool IsUsed(false);

        // 이미 사용된 네임스페이스 인지 체크.
        for (wstring& Check : _vecUsedNamespaces)
        {
            if (OutName == Check)
            {
                IsUsed = true;
                break;
            }
        }

        if (false == IsUsed)
        {
            wstring UseNamespaceStr(CPP_HandlNamespaceUse);
            REPLACE(UseNamespaceStr, CPP_IDF_NAMESPACE, OutName);
            WritePacketHeaders(UseNamespaceStr);

            // 사용된 네임스페이스 목록에 추가.
            _vecUsedNamespaces.push_back(OutName);
        }
    }
}

bool CppHandlerGenerator::GenerateHandlH()
{
    wstring CurTime(GET_TIME());

    wstring CommentClassOpen(CPP_HandlHCommentClassOpen);
    REPLACE(CommentClassOpen, CPP_IDF_DATE, CurTime);
    WriteHandlH(CommentClassOpen);

    EnumData* pData(GetMessageTypeData());
    if (nullptr == pData)
    {
        PRINT(L"MessageType 이름을 가진 열거자가 필요 합니다. 이 열거자는 패킷 이름으로 사용합니다." << endl);
        return false;
    }

    for (wstring& EnumData : pData->_EnumDatas)
    {
        wstring Protocol(GetProtocolNameFromMessageTypeEnum(EnumData));

        wstring VirtualFuncStr(CPP_HandlPacketVirtualFunc);
        REPLACE(VirtualFuncStr, CPP_IDF_PROTOCOL_NAME, Protocol);
        REPLACE(VirtualFuncStr, CPP_IDF_ENUM_DATA_NAME, EnumData);
        WriteHandlH(VirtualFuncStr);
    }

    WriteHandlH(CPP_HandlHClassClose);

    return true;
}

bool CppHandlerGenerator::GenerateHandlCpp()
{
    wstring CurrTime(GET_TIME());

    wstring CommentClassOpen(CPPS_HandlCppCommentClassOpen);
    REPLACE(CommentClassOpen, CPP_IDF_DATE, CurrTime);
    WriteHandlCpp(CommentClassOpen);

    WriteHandlCpp(CPPS_HandlCppConstructorDefaultFunc);

    EnumData* pData(GetMessageTypeData());
    if (nullptr == pData)
    {
        PRINT(L"MessageType 이름을 가진 열거자가 필요 합니다. 이 열거자는 패킷 이름으로 사용합니다." << endl);
        return false;
    }

    WriteHandlCpp(CPPS_HandlCppOnMessageOpen);    

    for (wstring& EnumData : pData->_EnumDatas)
    {
        wstring Protocol(GetProtocolNameFromMessageTypeEnum(EnumData));

        wstring CaseStr(CPPS_HandlCppOnMessageContinue);
        REPLACE(CaseStr, CPP_IDF_P1, EnumData);
        REPLACE(CaseStr, CPP_IDF_P2, Protocol);
        WriteHandlCpp(CaseStr);
    }    

    WriteHandlCpp(CPPS_HandlCppOnMessageClose);
    WriteHandlCpp(CPPS_FunctionClose);

    WriteHandlCpp(CPPS_HandlCppEncrypt_Open);
    for (auto* pParser : _vecParsers)
    {
        for (auto& Data : pParser->GetParsedData()->_Structs)
        {
            if (true == Data._IsEncrypt)
            {
                wstring CaseStr(CPPS_HandlCppEncryptDecrypt_Case);
                REPLACE(CaseStr, CPP_IDF_P1, Data._StructName);
                WriteHandlCpp(CaseStr);
            }
        }
    }
    wstring EncryptClose(CPPS_HandlCppEncryptDecrypt_Close);
    wstring BitCode(GetEncryptBitCodeStr());
    if (0 >= BitCode.size())
    {
        PRINT(L"EncryptBitCode 값이 설정 되지 않았습니다." << endl);
        return false;
    }
    REPLACE(EncryptClose, CPP_IDF_P1, BitCode);
    WriteHandlCpp(EncryptClose);

    WriteHandlCpp(CPPS_HandlCppDecrypt_Open);
    for (auto* pParser : _vecParsers)
    {
        for (auto& Data : pParser->GetParsedData()->_Structs)
        {
            if (true == Data._IsEncrypt)
            {
                wstring CaseStr(CPPS_HandlCppEncryptDecrypt_Case);
                REPLACE(CaseStr, CPP_IDF_P1, Data._StructName);
                WriteHandlCpp(CaseStr);
            }
        }
    }
    wstring DecryptClose(CPPS_HandlCppEncryptDecrypt_Close);
    REPLACE(DecryptClose, CPP_IDF_P1, BitCode);
    WriteHandlCpp(DecryptClose);

    return true;
}

EnumData* CppHandlerGenerator::GetMessageTypeData()
{
    // 파싱된 것중에 MessageType 열거자 선언을 찾는다.
    for (auto* pParser : _vecParsers)
    {
        for (auto& Data : pParser->GetParsedData()->_Enums)
        {
            if (PACKET_MESSAGE_TYPE_NAME == Data._Name)
            {
                return &Data;
            }
        }
    }

    return nullptr;
}

wstring CppHandlerGenerator::GetProtocolNameFromMessageTypeEnum(wstring& EnumData)
{
    if (0 >= EnumData.size())               { return wstring(); }

    wchar_t FirstCh(EnumData[0]);

    // 소문자 글자 이면 첫글자를 지운다.
    if (L'a' <= FirstCh && L'z' >= FirstCh)
    {
        wstring Clone(EnumData.begin() + 1, EnumData.end());
        return Clone;
    }
    // 아니면 그대로 리턴.
    return EnumData;
}

wstring CppHandlerGenerator::GetEncryptBitCodeStr()
{
    for (auto* pParser : _vecParsers)
    {
        if (0 < pParser->GetParsedData()->_EncryptBitCode.size())
        {
            return pParser->GetParsedData()->_EncryptBitCode;
        }
    }
    return L"";
}

void CppHandlerGenerator::WritePacketHeaders(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pFPPacketHeaders);
}

void CppHandlerGenerator::WriteHandlH(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pFPHandlH);
}

void CppHandlerGenerator::WriteHandlCpp(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pFPHandlCpp);
}

