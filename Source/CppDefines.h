#pragma once

#define OUT_EXT_CPP                     L".cpp"
#define OUT_EXT_H                       L".h"

#define OUT_HANDL_PACKET_HEADERS        L"PacketHeaders.h"
#define OUT_HANDL_H                     L"PacketHandler.h"
#define OUT_HANDL_CPP                   L"PacketHandler.cpp"

// 자료형.
#define CPP_TY_I64                      L"INT64"
#define CPP_TY_I32                      L"int"
#define CPP_TY_I16                      L"short"
#define CPP_TY_UI64                     L"UINT64"
#define CPP_TY_UI32                     L"UINT"
#define CPP_TY_UI16                     L"USHORT"
#define CPP_TY_I8                       L"byte"
#define CPP_TY_f64                      L"double"
#define CPP_TY_f32                      L"float"
//#define CPP_TY_f16                      L"f16"
#define CPP_TY_STR                      L"string"
#define CPP_TY_WSTR                     L"wstring"
#define CPP_TY_LIST                     L"vector"


#define CPP_TAB_STR                     L"    "

#define CPP_IDF_NAMESPACE               L"!NAMESPACE!"
#define CPP_IDF_DATE                    L"!YYYYMMDD!"
#define CPP_IDF_PACKET_NAME             L"!PACKET_NAME!"
#define CPP_IDF_PACKET_F_NAME           L"!PACKET_FILE_NAME!"
#define CPP_IDF_INCLUDE_NAME            L"!INCLUDE_NAME!"

// 진작 이렇게 쓸걸.. 그냥 파라메터 이름 구분용. 이전에 만든거는 어쩔 수 없이 그냥 둔다.
#define CPP_IDF_P1                      L"@P1@"
#define CPP_IDF_P2                      L"@P2@"
#define CPP_IDF_P3                      L"@P3@"


// 구조체. .h
#define CPP_IDF_STRUCT_NAME             L"!STRUCT_NAME!"

#define CPP_IDF_STRUCT_VAR_TY           L"%STRUCT_VAR_TYPE%"    // 자료형 선언 이름. 
#define CPP_IDF_STRUCT_VAR_NAME         L"%STRUCT_VAR_NAME%"    // 멤버 변수이름. 배열 까지 붙음.

#define CPP_IDF_STRUCT_CONS_LIST        L"%STRUCT_CONSTRUCT_MEMBER_LIST%"

// 구조체. .cpp
#define CPP_IDF_STRUCT_MEMBER_NAME      L"#MEMBER_NAME#"
#define CPP_IDF_STRUCT_ARR_MEMBER_CNT   L"#STRUCT_MEMBER_ARRAY_CNT#"
#define CPP_IDF_STRUCT_MEMBER_NAME_IN_ARRAY L"#MEMBER_TYPE_IN_ARRAY#"


// 열거자. .h
#define CPP_IDF_ENUM_NAME               L"!ENUM_NAME!"

// 열거자. .cpp
#define CPP_IDF_ENUM_DATA_NAME          L"#ENUM_DATA_NAME#"


// 패킷 핸들러 관련.
#define CPP_IDF_PACKET_FILE_NAME        L"!PACKET_FILE!"
#define CPP_IDF_NAMESPACE_FULL          L"!NAMESPACE_FULL_STR!"
#define CPP_IDF_PROTOCOL_NAME           L"!PROTOCOL_NAME!"
