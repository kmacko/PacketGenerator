#include "stdafx.h"
#include "CppDefines.h"
#include "CPP_SRC_CPP.h"
#include "CPP_SRC_H.h"
#include "TypeParser.h"
#include "CppGenerator.h"


CppGenerator::CppGenerator()
    : _pParsedData(nullptr),
    _pHeaderFile(nullptr),
    _pCppFile(nullptr)

{
}

CppGenerator::~CppGenerator()
{
}

void CppGenerator::Initialize(const wchar_t* pOutPath, TypeParser* pParser)
{
    _OutFilePath = GetOutputPath(pOutPath);

    _PacketFileNameNoExt = pParser->GetPacketFileName();
    _PacketFileName = _PacketFileNameNoExt + PACKET_FILE_EXT;
    _OutFilePath += _PacketFileNameNoExt;

    _pParsedData = pParser->GetParsedData();
    _mapIncludes = pParser->GetIncludesData();      // 컨테이너 복사.
}

bool CppGenerator::GenerateSource()
{
    if (false == OpenFiles(_OutFilePath.c_str()))
    {
        FileClose();
        return false;
    }

    GenerateHeader();
    GenerateCpp();

    FileClose();

    return true;
}

wstring CppGenerator::GetOutputPath(const wchar_t* pOutPath)
{
    wstring FinalPath(pOutPath);
    wchar_t LastChar(FinalPath[FinalPath.size() - 1]);

    // 마지막 글자가 \\, / 가 아니면 \\ 붙인다.
    if (L'\\' != LastChar || L'/' != LastChar)
    {
        FinalPath += L'\\';
    }

    return FinalPath;
}

bool CppGenerator::OpenFiles(const wchar_t* pOutPath)
{
    wstring CppFile(pOutPath);
    wstring HFile(pOutPath);

    CppFile += OUT_EXT_CPP;
    HFile += OUT_EXT_H;

    auto* cpCppFile(WC2C(CppFile.c_str()));
    if (0 != fopen_s(&_pCppFile, cpCppFile, "w+"))
    {
        PRINT(L"Cpp 파일 쓰기모드 열기 실패! - " << CppFile << endl);
        return false;
    }

    auto* cpHeaderFile(WC2C(HFile.c_str()));
    if (0 != fopen_s(&_pHeaderFile, cpHeaderFile, "w+"))
    {
        PRINT(L"Header 파일 쓰기모드 열기 실패! - " << HFile << endl);
        return false;
    }

    return true;
}

void CppGenerator::FileClose()
{
    if (nullptr != _pCppFile)
    {
        fclose(_pCppFile);
        _pCppFile = nullptr;
    }
    if (nullptr != _pHeaderFile)
    {
        fclose(_pHeaderFile);
        _pHeaderFile = nullptr;
    }
}

void CppGenerator::GenerateHeader()
{
    wstring CurTime(GET_TIME());

    // 1. 주석.
    wstring HeaderStr(CPP_Header);
    REPLACE(HeaderStr, CPP_IDF_PACKET_NAME, _PacketFileNameNoExt);
    REPLACE(HeaderStr, CPP_IDF_DATE, CurTime);
    REPLACE(HeaderStr, CPP_IDF_PACKET_F_NAME, _PacketFileName);
    WriteHeader(HeaderStr);

    // 2. 헤더. Include 목록.
    for (auto& Iter : _mapIncludes)
    {
        wstring IncludeName(Iter.first);
        wstring IncludeStr(CPP_IncludeHeader);
        REPLACE(IncludeStr, CPP_IDF_INCLUDE_NAME, IncludeName);
        WriteHeader(IncludeStr);
    }

    WriteHeader(wstring(L"\n"));

    // 3. 네임스페이스.
    for (wstring& Namespace : _pParsedData->_NamespaceData)
    {
        wstring NamespaceStr(CPP_Namespace);
        REPLACE(NamespaceStr, CPP_IDF_NAMESPACE, Namespace);
        WriteHeader(NamespaceStr);
    }

    // 4. 열거자.
    GenerateEnumsToHeader();

    // 열거자 구조체 사이 엔터 두번.
    if (0 < _pParsedData->_Enums.size() && 0 < _pParsedData->_Structs.size())
    {
        WriteHeader(wstring(L"\n\n"));
    }

    // 5. 구조체.
    GenerateStructToHeader();

    // 6. 네임스페이스 닫기.
    for (size_t End(_pParsedData->_NamespaceData.size()) ; 0 < End ; --End)
    {
        wstring NameSpace(_pParsedData->_NamespaceData[End - 1]);
        wstring NamespaceClose(CPP_NamespaceClose);
        REPLACE(NamespaceClose, CPP_IDF_NAMESPACE, NameSpace);
        WriteHeader(NamespaceClose);
    }
}

void CppGenerator::GenerateEnumsToHeader()
{
    for(size_t EnumCnt(0) ; _pParsedData->_Enums.size() > EnumCnt ; ++EnumCnt)
    {
        auto& Data(_pParsedData->_Enums[EnumCnt]);

        wstring EnumStart(CPP_EnumStart);
        REPLACE(EnumStart, CPP_IDF_ENUM_NAME, Data._Name);
        WriteHeader(EnumStart);

        for (size_t i(0) ; Data._EnumDatas.size() > i ; ++i)
        {
            int EnumValue(Data._EnumValues[i]);
            wstring EnumData(Data._EnumDatas[i]);

            wchar_t Buff[MAX_PATH] = { 0 };
            if (INT_MAX == EnumValue)
            {
                wsprintf(Buff, CPP_EnumDeclearFormat.c_str(), EnumData.c_str());
            }
            else
            {
                wsprintf(Buff, CPP_EnumDeclearFormatWithValue.c_str(), EnumData.c_str(), EnumValue);
            }

            WriteHeader(wstring(Buff));
        }

        wstring EndStr(CPP_EnumEnd);
        REPLACE(EndStr, CPP_IDF_ENUM_NAME, Data._Name);
        WriteHeader(EndStr);

        if (EnumCnt < _pParsedData->_Enums.size() - 1)
        {
            WriteHeader(wstring(L"\n\n"));
        }
    }
}

void CppGenerator::GenerateStructToHeader()
{
    for (size_t StructCnt(0) ; _pParsedData->_Structs.size() > StructCnt ; ++StructCnt)
    {
        auto& Data(_pParsedData->_Structs[StructCnt]);

        wstring StructStart(CPP_StructStart);
        REPLACE(StructStart, CPP_IDF_STRUCT_NAME, Data._StructName);
        WriteHeader(StructStart);

        vector<wstring> FinTypeList;
        for (size_t i(0) ; Data._MemberTypes.size() > i ; ++i)
        {
            int ArrCnt(Data._MemberArrayCnt[i]);        // 배열 선언이 아니면 1.
            wstring TypeName(Data._MemberTypes[i]);
            wstring MemberName(Data._MemberNames[i]);
            wstring ContainerName(Data._MemberContainerName[i]);

            wstring TypeStr(GetTypeName(ContainerName, TypeName));
            wstring FinalMemberName(GetMemberName(MemberName, ArrCnt));
            FinTypeList.push_back(TypeStr);

            wstring MemberDeclear(CPP_StructMember);
            REPLACE(MemberDeclear, CPP_IDF_STRUCT_VAR_TY, TypeStr);
            REPLACE(MemberDeclear, CPP_IDF_STRUCT_VAR_NAME, FinalMemberName);

            WriteHeader(MemberDeclear);
        }

        // 이 구조체가 MessageType에 포함된 것이면 TYPE 인라인 함수를 넣어준다.
        if (true == IsProtoalStructName(Data._StructName))
        {
            wstring strMessageType(CPP_MessageType);
            REPLACE(strMessageType, CPP_IDF_P1, Data._StructName);
            WriteHeader(strMessageType);
        }

        wstring StructConstruct1(CPP_StructConstructor1);
        REPLACE(StructConstruct1, CPP_IDF_STRUCT_NAME, Data._StructName);
        WriteHeader(StructConstruct1);

        if (0 < Data._MemberTypes.size())
        {
            wstring MemberList(GetConstructMemberListStr(&Data, FinTypeList));

            wstring StructConstruct2(CPP_StructConstructor2);

            REPLACE(StructConstruct2, CPP_IDF_STRUCT_NAME, Data._StructName);
            REPLACE(StructConstruct2, CPP_IDF_STRUCT_CONS_LIST, MemberList);
            WriteHeader(StructConstruct2);
        }

        wstring StructConstruct3(CPP_StructConstructor3);
        REPLACE(StructConstruct3, CPP_IDF_STRUCT_NAME, Data._StructName);
        WriteHeader(StructConstruct3);

        if (StructCnt < _pParsedData->_Structs.size() - 1)
        {
            WriteHeader(wstring(L"\n\n"));
        }
    }
}

void CppGenerator::GenerateCpp()
{
    wstring CurTime(GET_TIME());

    // 1. 주석.
    wstring HeaderStr(CPPS_Header);
    REPLACE(HeaderStr, CPP_IDF_PACKET_NAME, _PacketFileNameNoExt);
    REPLACE(HeaderStr, CPP_IDF_DATE, CurTime);
    REPLACE(HeaderStr, CPP_IDF_PACKET_F_NAME, _PacketFileName);
    WriteCpp(HeaderStr);

    // 2. 네임스페이스.
    for (wstring& Namespace : _pParsedData->_NamespaceData)
    {
        wstring NamespaceStr(CPP_Namespace);
        REPLACE(NamespaceStr, CPP_IDF_NAMESPACE, Namespace);
        WriteCpp(NamespaceStr);
    }

    // 3. 열거자.
    GenerateEnumsToCpp();

    // 열거자 구조체 사이 엔터 두번.
    if (0 < _pParsedData->_Enums.size() && 0 < _pParsedData->_Structs.size())
    {
        WriteCpp(wstring(L"\n\n"));
    }

    // 4. 구조체.
    GenerateStructToCpp();

    // 5. 네임스페이스 닫기.
    for (size_t End(_pParsedData->_NamespaceData.size()) ; 0 < End ; --End)
    {
        wstring NameSpace(_pParsedData->_NamespaceData[End - 1]);
        wstring NamespaceClose(CPP_NamespaceClose);
        REPLACE(NamespaceClose, CPP_IDF_NAMESPACE, NameSpace);
        WriteCpp(NamespaceClose);
    }
}

void CppGenerator::GenerateEnumsToCpp()
{
    for (auto& Data : _pParsedData->_Enums)
    {
        wstring Declear(CPPS_EnumStrDeclear);
        REPLACE(Declear, CPP_IDF_ENUM_NAME, Data._Name);
        WriteCpp(Declear);

        for (wstring& DataName : Data._EnumDatas)
        {
            wstring DeclearData(CPPS_EnumStrDeclearData);
            REPLACE(DeclearData, CPP_IDF_ENUM_DATA_NAME, DataName);
            WriteCpp(DeclearData);
        }

        wstring DeclearEnd(CPPS_EnumStrDeclearEnd);
        REPLACE(DeclearEnd, CPP_IDF_ENUM_NAME, Data._Name);
        WriteCpp(DeclearEnd);

        // 마지막 게 아니면 엔터 두번.
        if (Data._Name != _pParsedData->_Enums.rbegin()->_Name)
        {
            WriteCpp(L"\n\n");
        }
    }
}

void CppGenerator::GenerateStructToCpp()
{
    for (size_t StructCnt(0) ; _pParsedData->_Structs.size() > StructCnt ; ++StructCnt)
    {
        auto& Data(_pParsedData->_Structs[StructCnt]);

        GenerateStructToCppOperatorFunc(&Data);
        WriteCpp(wstring(L"\n"));

        // 기본생성자.
        wstring DefaultCons(CPPS_Constructor1);
        REPLACE(DefaultCons, CPP_IDF_STRUCT_NAME, Data._StructName);
        WriteCpp(DefaultCons);
        WriteCpp(wstring(L"\n"));

        if (0 < Data._MemberTypes.size())
        {
            GenerateStructToCppConstructWithMembers(&Data);
            WriteCpp(wstring(L"\n"));
        }

        // 패킷 기반 생성자.
        wstring ConstructFromPacket(CPPS_Constructor3);
        REPLACE(ConstructFromPacket, CPP_IDF_STRUCT_NAME, Data._StructName);
        WriteCpp(ConstructFromPacket);
        WriteCpp(wstring(L"\n"));

        GenerateStructToCppSetData(&Data);
        WriteCpp(wstring(L"\n"));

        GenerateStructToCppSetPacket(&Data);

        if (StructCnt < _pParsedData->_Structs.size() - 1)
        {
            WriteCpp(wstring(L"\n\n"));
        }
    }
}

void CppGenerator::GenerateStructToCppOperatorFunc(StructData* pData)
{
    wstring OperatorOpen(CPPS_OperatorOpen);
    REPLACE(OperatorOpen, CPP_IDF_STRUCT_NAME, pData->_StructName);
    WriteCpp(OperatorOpen);

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring OperatorMember((1 < pData->_MemberArrayCnt[i]) ? CPPS_OperatorMemberArray : CPPS_OperatorMember);
        
        REPLACE(OperatorMember, CPP_IDF_STRUCT_MEMBER_NAME, pData->_MemberNames[i]);
        REPLACE(OperatorMember, CPP_IDF_STRUCT_ARR_MEMBER_CNT, ITOW(pData->_MemberArrayCnt[i]));
        WriteCpp(OperatorMember);
    }

    WriteCpp(CPPS_OperatorClose);
}

void CppGenerator::GenerateStructToCppConstructWithMembers(StructData* pData)
{
    vector<wstring> FinTypeList;
    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        FinTypeList.push_back(GetTypeName(pData->_MemberContainerName[i], pData->_MemberTypes[i]));
    }

    wstring MemberListStr(GetConstructMemberListStr(pData, FinTypeList));

    wstring ConstructMemberList(CPPS_Constructor2);
    REPLACE(ConstructMemberList, CPP_IDF_STRUCT_NAME, pData->_StructName);
    REPLACE(ConstructMemberList, CPP_IDF_STRUCT_CONS_LIST, MemberListStr);
    WriteCpp(ConstructMemberList);

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring MemberInit((1 < pData->_MemberArrayCnt[i]) ? CPPS_MemberArrayInit : CPPS_MemberInit);

        REPLACE(MemberInit, CPP_IDF_STRUCT_MEMBER_NAME, pData->_MemberNames[i]);
        REPLACE(MemberInit, CPP_IDF_STRUCT_ARR_MEMBER_CNT, ITOW(pData->_MemberArrayCnt[i]));

        WriteCpp(MemberInit);
    }

    WriteCpp(CPPS_FunctionClose);    
}

void CppGenerator::GenerateStructToCppSetData(StructData* pData)
{
    wstring SetDataOpen(CPPS_SetData);
    REPLACE(SetDataOpen, CPP_IDF_STRUCT_NAME, pData->_StructName);
    WriteCpp(SetDataOpen);

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        // 자료형 확인.
        wstring Type(pData->_MemberTypes[i]);
        wstring Container(pData->_MemberContainerName[i]);
        int ArrCnt(pData->_MemberArrayCnt[i]);

        wstring MemberInit;

        // 컨테이너 자료형 적용.
        if (0 < Container.size())
        {
            if (TYPE_LIST == Container)
            {
                // 기본 자료형들.
                if (true == IsDefaultTypeName(Type))    { MemberInit = CPPS_SetDataMemberListDefault; }
                else if (TYPE_STR == Type)              { MemberInit = CPPS_SetDataMemberListStr; }
                else if (TYPE_WSTR == Type)             { MemberInit = CPPS_SetDataMemberListWStr; }
                // 열거자 자료형.
                else if (true == IsEnumTypeName(Type))  { MemberInit = CPPS_SetDataMemberListEnum; }
                // 구조체 자료형.
                else                                    { MemberInit = CPPS_SetDataMemberListCustom; }
            }
            else
            {
                PRINT(L"알 수 없는 컨테이너 자료형 입니다! :" << Container << endl);
            }
        }
        // 일반 자료형 초기화.
        else
        {
            // 배열. 배열은 기본 자료형 외에 적용 안됨.
            if (1 < ArrCnt)                             { MemberInit = CPPS_SetDataMemberArray; }
            else
            {
                // 기본 자료형들.
                if (true == IsDefaultTypeName(Type))    { MemberInit = CPPS_SetDataMemberDefault; }
                else if (TYPE_STR == Type)              { MemberInit = CPPS_SetDataMemberStr; }
                else if (TYPE_WSTR == Type)             { MemberInit = CPPS_SetDataMemberWstr; }
                // 열거자 자료형.
                else if (true == IsEnumTypeName(Type))  { MemberInit = CPPS_SetDataMemberEnum; }
                // 구조체.
                else                                    { MemberInit = CPPS_SetDataMemberCustom; }
            }
        }

        REPLACE(MemberInit, CPP_IDF_STRUCT_MEMBER_NAME, pData->_MemberNames[i]);
        REPLACE(MemberInit, CPP_IDF_STRUCT_MEMBER_NAME_IN_ARRAY, GetTypeName(Type));
        REPLACE(MemberInit, CPP_IDF_ENUM_NAME, pData->_MemberTypes[i]);
        REPLACE(MemberInit, CPP_IDF_STRUCT_ARR_MEMBER_CNT, ITOW(ArrCnt));
        WriteCpp(MemberInit);
    }
    WriteCpp(CPPS_FunctionClose);
}

void CppGenerator::GenerateStructToCppSetPacket(StructData* pData)
{
    wstring SetDataOpen(CPPS_SetPacket);
    REPLACE(SetDataOpen, CPP_IDF_STRUCT_NAME, pData->_StructName);
    WriteCpp(SetDataOpen);

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        // 자료형 확인.
        wstring Type(pData->_MemberTypes[i]);
        wstring Container(pData->_MemberContainerName[i]);
        int ArrCnt(pData->_MemberArrayCnt[i]);

        wstring MemberInit;

        // 컨테이너 자료형 적용.
        if (0 < Container.size())
        {
            if (TYPE_LIST == Container)
            {
                // 기본 자료형들.
                if (true == IsDefaultTypeName(Type))            { MemberInit = CPPS_SetPacketMemberListDefault; }
                else if (TYPE_STR == Type || TYPE_WSTR == Type) { MemberInit = CPPS_SetPacketMemberListWStrAndStr; }
                // 열거자 자료형.
                else if (true == IsEnumTypeName(Type))          { MemberInit = CPPS_SetPacketMemberListEnum; }
                // 구조체 자료형.
                else                                            { MemberInit = CPPS_SetPacketMemberListCustom; }
            }
            else
            {
                PRINT(L"알 수 없는 컨테이너 자료형 입니다! :" << Container << endl);
            }
        }
        // 일반 자료형 초기화.
        else
        {
            // 배열. 배열은 기본 자료형 외에 적용 안됨.
            if (1 < ArrCnt)                                     { MemberInit = CPPS_SetPacketMemberArray; }
            else
            {
                // 기본 자료형들.
                if (true == IsDefaultTypeName(Type))            { MemberInit = CPPS_SetPacketMemberDefault; }
                else if (TYPE_STR == Type || TYPE_WSTR == Type) { MemberInit = CPPS_SetPacketMemberWStrAndStr; }
                // 열거자 자료형.
                else if (true == IsEnumTypeName(Type))          { MemberInit = CPPS_SetPacketMemberEnum; }
                // 구조체 자료형.
                else                                            { MemberInit = CPPS_SetPacketMemberCustom; }
            }
        }

        REPLACE(MemberInit, CPP_IDF_STRUCT_MEMBER_NAME, pData->_MemberNames[i]);
        REPLACE(MemberInit, CPP_IDF_STRUCT_MEMBER_NAME_IN_ARRAY, Type);
        REPLACE(MemberInit, CPP_IDF_STRUCT_ARR_MEMBER_CNT, ITOW(ArrCnt));
        WriteCpp(MemberInit);
    }
    WriteCpp(CPPS_FunctionClose);
}

wstring CppGenerator::GetTypeName(wstring& Container, wstring& Type)
{
    wstring ContainerName;
    if (0 < Container.size())
    {
        if (TYPE_LIST == Container)     { ContainerName = CPP_TY_LIST; }
        else
        {
            PRINT(L"오류! 사용 불가능한 컨테이너 자료형 입니다. :" << Container << endl);
            return wstring();
        }
    }

    wstring TypeName(GetTypeName(Type));

    if (0 < ContainerName.size())
    {
        wstring Format(CPP_StructMemberDeclearContainerFormat);
        REPLACE(Format, CPP_IDF_P1, ContainerName);
        REPLACE(Format, CPP_IDF_P2, TypeName);
        return Format;
    }

    return TypeName;
}

wstring CppGenerator::GetTypeName(wstring& Type)
{
    if (TYPE_I64 == Type)               { return CPP_TY_I64; }
    else if (TYPE_I32 == Type)          { return CPP_TY_I32; }
    else if (TYPE_I16 == Type)          { return CPP_TY_I16; }
    else if (TYPE_I8 == Type)           { return CPP_TY_I8; }
    else if (TYPE_f64 == Type)          { return CPP_TY_f64; }
    else if (TYPE_f32 == Type)          { return CPP_TY_f32; }
    else if (TYPE_UI64 == Type)         { return CPP_TY_UI64; }
    else if (TYPE_UI32 == Type)         { return CPP_TY_UI32; }
    else if (TYPE_UI16 == Type)         { return CPP_TY_UI16; }
    else if (TYPE_STR == Type)          { return CPP_TY_STR; }
    else if (TYPE_WSTR == Type)         { return CPP_TY_WSTR; }
    else                                { return Type; }        // 구조체, 열거자로 정의된 자료형.
}

wstring CppGenerator::GetMemberName(wstring& Name, int ArrayCnt)
{
    if (1 < ArrayCnt)
    {
        wstring Format(CPP_MemberNameDeclearArrayFormat);
        REPLACE(Format, CPP_IDF_P1, Name);
        REPLACE(Format, CPP_IDF_P2, ITOW(ArrayCnt));
        return Format;
    }
    
    return Name;
}

wstring CppGenerator::GetConstructMemberListStr(StructData* pData, vector<wstring>& FinalTypes)
{
    wstring RetStr(L"");
    for (size_t i(0) ; FinalTypes.size() > i ; ++i)
    {
        wstring Format;

        if (1 < pData->_MemberArrayCnt[i])                              // 배열. 포인터 파라메터.
        {
            Format = CPP_StructConstructMemberParamListArrayFormat;
        }
        else if (0 < pData->_MemberContainerName[i].size())             // 컨테이너. 참조 파라메터.
        {
            Format = CPP_StructConstructMemberParamListRefFormat;
        }
        else if (true == IsDefaultTypeName(pData->_MemberTypes[i]))     // 기본 자료형.
        {
            Format = CPP_StructConstructMemberParamListValueFormat;
        }
        else if (true == IsEnumTypeName(pData->_MemberTypes[i]))        // 열거자. 값 파라메터.
        {
            Format = CPP_StructConstructMemberParamListValueFormat;
        }
        else                                                            // 나머지 구조체. 참조 파라메터.
        {
            Format = CPP_StructConstructMemberParamListRefFormat;
        }

        REPLACE(Format, CPP_IDF_P2, FinalTypes[i]);
        REPLACE(Format, CPP_IDF_P3, pData->_MemberNames[i]);
        REPLACE(Format, CPP_IDF_P1, RetStr);
        RetStr = Format;

        // 마지막 멤버가 아니면 뒤에 ", " 추가.
        if (i < FinalTypes.size() - 1)
        {
            RetStr += L", ";
        }
    }

    return RetStr;
}

bool CppGenerator::IsEnumTypeName(wstring& Type)
{
    // 열거자 목록에 있는지 조회 한다.
    for (auto& n : _pParsedData->_Enums)
    {
        if (n._Name == Type)
        {
            return true;
        }
    }

    // 없으면 Include 목록에서 조회.
    for (auto& iter : _mapIncludes)
    {
        for (auto& n : iter.second->_Enums)
        {
            if (n._Name == Type)
            {
                return true;
            }
        }
    }
    return false;
}

bool CppGenerator::IsDefaultTypeName(wstring& Type)
{
    if (TYPE_I64 == Type    || TYPE_I32 == Type     || TYPE_I16 == Type     ||
        TYPE_UI64 == Type   || TYPE_UI32 == Type    || TYPE_UI16 == Type    ||
        TYPE_I8 == Type     || TYPE_f64 == Type     || TYPE_f32 == Type)
    {
        return true;
    }
    return false;
}

bool CppGenerator::IsProtoalStructName(wstring& StructName)
{
    for (auto& Iter : _mapIncludes)
    {
        auto& IncludeData(Iter.second);
        
        for (auto& Enum : IncludeData->_Enums)
        {
            if (PACKET_MESSAGE_TYPE_NAME == Enum._Name)
            {
                for (auto& Name : Enum._EnumDatas)
                {
                    if (StructName == GetProtocolNameFromMessageTypeEnum(Name))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    return false;
}

wstring CppGenerator::GetProtocolNameFromMessageTypeEnum(wstring& EnumData)
{
    if (0 >= EnumData.size()) { return wstring(); }

    wchar_t FirstCh(EnumData[0]);

    // 소문자 글자 이면 첫글자를 지운다.
    if (L'a' <= FirstCh && L'z' >= FirstCh)
    {
        wstring Clone(EnumData.begin() + 1, EnumData.end());
        return Clone;
    }
    // 아니면 그대로 리턴.
    return EnumData;
}

void CppGenerator::WriteHeader(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pHeaderFile);
}

void CppGenerator::WriteCpp(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pCppFile);
}
