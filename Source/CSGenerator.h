#pragma once

class TypeParser;
class StructData;

class CSGenerator
{
public :
    CSGenerator();
    ~CSGenerator();

public :
    void                        Initialize(const wchar_t* pOutPath, TypeParser* pParser);
    bool                        GenerateSource();

private :
    bool                        GenerateEnums();
    bool                        GenerateStructs();
    bool                        WriteMemberDeclear(StructData* pData);
    bool                        WriteMemberParamConstructor(StructData* pData);
    bool                        WriteSetDataFunc(StructData* pData);
    bool                        WriteSetPacketFunc(StructData* pData);

    wstring                     GetOutputPath(const wchar_t* pOutPath);
    wstring                     GetContainerName(wstring& OrigContainer);
    wstring                     GetTypeName(wstring& Type);
    wstring                     FirstEnumName(wstring& EnumName);
    wstring                     DefulatValueString(wstring& OrigType);

    wstring                     GetConstructMemberListStr(StructData* pData);
    bool                        IsEnumTypeName(wstring& Type);
    bool                        IsDefaultTypeName(wstring& Type);
    wstring                     GetMemberDeclearFormat(size_t MemberIndex, StructData* pData);
    wstring                     GetMemberParamListFormat(size_t MemberIndex, StructData* pData);
    wstring                     GetMemberSetDataFormat(size_t MemberIndex, StructData* pData);
    wstring                     GetMemberSetDataContainerListFormat(size_t MemberIndex, StructData* pData);
    wstring                     GetMemberSetPacketContainerListFormat(size_t MemberIndex, StructData* pData);
    wstring                     GetMemberSetPacketFormat(size_t MemberIndex, StructData* pData);

    bool                        OpenFile(const wchar_t* pOutPath);
    void                        FileClose();
    void                        WriteCS(const wstring& Str);

private :
    TotalParsedData*            _pParsedData;
    MAP_Includes                _mapIncludes;

    FILE*                       _pCSFile;

    wstring                     _OutFilePath;
    wstring                     _PacketFileName;
    wstring                     _PacketFileNameNoExt;
};

