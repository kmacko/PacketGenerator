#pragma once

class TypeParser;
class TotalParsedData;
class EnumData;

class CppHandlerGenerator
{
public :
    CppHandlerGenerator();
    ~CppHandlerGenerator();

public :
    bool                        Initialize(wstring& OutPath, vector<TypeParser*>& vecParser);
    bool                        Generate();

private :
    bool                        OpenFiles();
    void                        CloseFiles();

    bool                        GeneratePacketHeadersH();
    vector<wstring>             MakeNameSpaceList(TotalParsedData* pData);
    void                        WriteUseingNamespace(vector<wstring>& NamespaceList);

    bool                        GenerateHandlH();

    bool                        GenerateHandlCpp();

    EnumData*                   GetMessageTypeData();
    wstring                     GetProtocolNameFromMessageTypeEnum(wstring& EnumData);
    wstring                     GetEncryptBitCodeStr();

    void                        WritePacketHeaders(const wstring& Str);
    void                        WriteHandlH(const wstring& Str);
    void                        WriteHandlCpp(const wstring& Str);

private :
    vector<TypeParser*>         _vecParsers;

    vector<wstring>             _vecUsedNamespaces;

    wstring                     _OutPath;

    FILE*                       _pFPPacketHeaders;
    FILE*                       _pFPHandlH;
    FILE*                       _pFPHandlCpp;
};

