#pragma once

class NamespaceData
{
public :
    NamespaceData(wstring& Name);
    ~NamespaceData();


public :
    wstring                     _Name;
};


class EnumData
{
public :
    EnumData();
    EnumData(wstring& EnumName);
    ~EnumData();

public :
    vector<wstring>             _EnumDatas;
    vector<int>                 _EnumValues;
    wstring                     _Name;
};


class StructData
{
public :
    StructData();
    StructData(wstring& StructName);
    ~StructData();

public :
    vector<wstring>             _MemberTypes;               // 멤버 변수 자료형. or 구조체 이름.
    vector<wstring>             _MemberNames;               // 멤버변수 이름.
    vector<int>                 _MemberArrayCnt;            // 배열 수. 기본 1
    vector<wstring>             _MemberContainerName;       // 컨테이너적용 이름. 기본 없음.

    wstring                     _StructName;
    bool                        _IsEncrypt;
};

class TotalParsedData
{
public :
    TotalParsedData();
    ~TotalParsedData();


public :
    void                        AddNamespaceData(wstring& NamespaceName);
    void                        AddEnumName(wstring& EnumName);
    void                        AddEnumData(wstring& DataName, int DataValue = INT_MAX);
    void                        AddNewStruct(wstring& StructName);
    void                        AddStructMember(wstring& TypeName, wstring& MemberName, int ArrayCnt, wstring& ContainerName);
    void                        SetStructEncrypt();

public :
    bool                        IsExistStructName(const wstring& StructName);

public :
    vector<wstring>             _NamespaceData;
    vector<EnumData>            _Enums;
    vector<StructData>          _Structs;
    wstring                     _EncryptBitCode;
};

// 컨테이너 자료형 선언.
typedef map<wstring, TotalParsedData*>              MAP_Includes;
typedef map<wstring, TotalParsedData*>::iterator    MAP_IncludesIter;
typedef map<wstring, TotalParsedData*>::value_type  MAP_IncludesVal;
