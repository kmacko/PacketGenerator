#include "stdafx.h"


char g_WC2CBuffer[8192] = { 0 };


char* WC2C(const wchar_t* str)
{
    //입력받은 wchar_t 변수의 길이를 구함
    int strSize = WideCharToMultiByte(CP_ACP, 0, str, -1, NULL, 0, NULL, NULL);

    //형 변환 
    WideCharToMultiByte(CP_ACP, 0, str, -1, g_WC2CBuffer, strSize, 0, 0);
    return g_WC2CBuffer;
}

void TOKEN(const wchar_t* In, const wchar_t Delim, std::vector<std::wstring> &Out)
{
    std::wstringstream ss(In);

    std::wstring s;
    while (std::getline(ss, s, Delim))
    {
        Out.push_back(s);
    }
}

void PRINTF(const wchar_t* Msg)
{
    printf_s(WC2C(Msg));
}

void PRINTF(const wstringstream& Msg)
{
    PRINTF(Msg.str());
}

void PRINTF(const wstring& Msg)
{
    PRINTF(Msg.c_str());
}

bool g_ContainsStr(const wstring& Target, const wchar_t* Find, bool AnyCase)
{
    wstring Clone(Target);

    if (true == AnyCase) { TOLOWER(Clone); }

    size_t Pos(Clone.find(Find));
    if (Clone.npos != Pos)
    {
        return true;
    }
    return false;
}

void g_RemoveStr(wstring& RefStr, const wchar_t* Find, bool AnyCase)
{
    wstring Clone(RefStr);

    if (true == AnyCase) { TOLOWER(Clone); }

    size_t Pos(Clone.find(Find));
    if (Clone.npos != Pos)
    {
        RefStr.erase(RefStr.begin() + Pos, RefStr.begin() + Pos + wcslen(Find));
    }
}
