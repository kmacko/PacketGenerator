#pragma once

// 암호화 여부. 대소문자 구분없음.
#define TYPE_ENCRYPT                L"encrypt"

// 자료형. 정확하게 입력해야 함.
#define TYPE_STRUCT                 L"struct"
#define TYPE_ENUM                   L"enum"

// 패킷 정의 파일에 이 열거자가 반드시 있어야 한다. 패킷 종류 목록으로 취급한다.
#define PACKET_MESSAGE_TYPE_NAME    L"MessageType"

#define TYPE_I64                    L"i64"
#define TYPE_I32                    L"i32"
#define TYPE_I16                    L"i16"
#define TYPE_UI64                   L"ui64"
#define TYPE_UI32                   L"ui32"
#define TYPE_UI16                   L"ui16"
#define TYPE_I8                     L"i8"
#define TYPE_f64                    L"f64"
#define TYPE_f32                    L"f32"
#define TYPE_f16                    L"f16"
#define TYPE_STR                    L"string"
#define TYPE_WSTR                   L"wstring"

#define TYPE_LIST                   L"list"
#define TYPE_LIST_OPEN              L"list<"
#define TYPE_LIST_CLOSE             L">"

#define ARRAY_OPEN                  L"["
#define ARRAY_CLOSE                 L"]"

#define TYPE_COMMENT                L"//"

#define TYPE_RANGE_COMMENT_BEGIN    L"/*"
#define TYPE_RANGE_COMMENT_END      L"*/"

#define TYPE_BRACE_OPEN             L"{"
#define TYPE_BRACE_CLOSE            L"}"

// 다른정의 포함 명령.
#define TYPE_INCLUDE                L"include"

#define TYPE_NAMESPACE              L"namespace"
#define TYPE_ENCRYPT_BIT            L"EncryptBitCode"

#define VLAUE_INPUT                 L"="


