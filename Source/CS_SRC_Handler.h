#pragma once

#include <string>
using namespace std;

/////////// ConnectorBase.cs
const wstring CS_HandlBaseCommment(L"\
///////////////////////////////////////////////////////////////\n\
/// ConnectorBase.cs\n\
/// @P1@ PacketGenerator.exe 에 의해 자동 생성됨.\n\
/// 코드 생성시 덮어 쓰므로 직접 수정금지.\n\
//////////////////////////////////////////////////////////////\n\
using UnityEngine;\n\
");

const wstring CS_HandlBaseNamespaceAddFormat(L"@P1@.@P2@");
const wstring CS_HandlBaseUseNamespace(L"using @P3@;\n");

const wstring CS_HandlBaseClassOpen(L"\
\n\
public partial class ConnectorBase : HNET.CHNetConnector\n\
{\n\
    public override void OnMessage(HNetPacket Packet)\n\
    {\n\
        switch ((MessageType)Packet.Type())\n\
        {\n\
");

// 프로토콜열거자이름@P1@, 열거자데이터@P2@, 열거자데이터기반프로토콜이름@P3@
const wstring CS_HandlBaseOnMessageCaseEnum(L"\
        case @P1@.@P2@: { OnRecv@P3@(Packet); } break;\n\
");

const wstring CS_HandlBaseOnMessageClose(L"\
        default: { OnRecvUnknownMessageType(Packet.Type()); } break;\n\
        }\n\
    }\n\
");

const wstring CS_HandlBaseSetCallBackOpen(L"\
\n\
    private void SetCallBack(MessageType Ty, System.Action<object> CB)\n\
    {\n\
        switch (Ty)\n\
        {\n\
");

// 프로토콜열거자이름@P1@, 열거자데이터@P2@, 열거자데이터기반프로토콜이름@P3@
const wstring CS_HandlBaseSetCallBackCaseEnum(L"\
        case @P1@.@P2@: { _OnRecv@P3@ = CB; } break;\n\
");

const wstring CS_HandlBaseSetCallBackClose(L"\
        default: { Debug.LogErrorFormat(\"알 수 없는 메세지 타입 입니다 - {0}\", (int)Ty); } break;\n\
        }\n\
    }\n\
\n\
    private void OnRecvUnDecleardMessage(MessageType Ty)\n\
    {\n\
        Debug.LogErrorFormat(\"정의 하지 않은 패킷 메세지가 있습니다 {0} ({1})\", Ty.ToString(), (int)Ty);\n\
    }\n\
\n\
    private void OnRecvUnknownMessageType(ushort Ty)\n\
    {\n\
        Debug.LogErrorFormat(\"확인되지 않은 패킷 메세지가 수신되었습니다 - {0}\", (int)Ty);\n\
    }\n\
");

// @P1@:패킷정의 열거자 이름.
const wstring CS_HandlBaseEncrypt_Open(L"\
\n\
    public override void OnEncrypt(ushort type, byte[] buf)\n\
    {\n\
        switch ((@P1@)type)\n\
        {\n\
");

// @P1@:패킷정의 열거자 이름.
const wstring CS_HandlBaseDecrypt_Open(L"\
\n\
    public override void OnDecrypt(ushort type, byte[] buf)\n\
    {\n\
        switch ((@P1@)type)\n\
        {\n\
");

// @P1@:패킷정의 열거자 이름.
// @P2@:암호화 옵션 걸린 구조체 이름.
const wstring CS_HandlBaseEncryptDecrypt_Case(L"\
        case @P1@.k@P2@:\n\
");

// @P1@:암호화 비트연산 값.
const wstring CS_HandlBaseEncryptDecrypt_Close(L"\
            break;\n\
        default: { return; }\n\
        }\n\
\n\
        for (ushort i = 0 ; buf.Length > i ; ++i)\n\
        {\n\
            buf[i] ^= @P1@;\n\
        }\n\
    }\n\
");

const wstring CS_PacketPartialClassEnd(L"\
}; // class ConnectorBase.\n\
/// EndOf @P1@\n\
");

/////////// Connector@P1@Base.cs
// 패킷정의파일@P1@, 시간@P2@
const wstring CS_HandlPartialComment(L"\
///////////////////////////////////////////////////////////////\n\
/// ConnectorBase.@P1@.cs\n\
/// @P2@ PacketGenerator.exe 에 의해 자동 생성됨.\n\
/// 코드 생성시 덮어 쓰므로 직접 수정금지.\n\
//////////////////////////////////////////////////////////////\n\
using UnityEngine;\n\
");

const wstring CS_HandlPartialClassOpen(L"\
\n\
public partial class ConnectorBase\n\
{\n\
");

// 프로토콜이름@P1@, 프로토콜 열거자이름@P2@, 프로토콜 열거자데이터이름@P3@
const wstring CS_HandlPartialPacketDeclear(L"\
    /// @P1@\n\
    public System.Action<@P1@> _OnRecv@P1@ = null;\n\
    public virtual void OnRecv@P1@(HNetPacket Packet)\n\
    {\n\
        if (null != _OnRecv@P1@)\n\
        {\n\
            @P1@ Data = new @P1@(Packet);\n\
            _OnRecv@P1@(Data);\n\
        }\n\
        else\n\
        {\n\
            OnRecvUnDecleardMessage((MessageType)Packet.Type());\n\
        }\n\
    }\n\
    public virtual void Send@P1@(@P1@ SendData, MessageType ResponseType = 0, System.Action<object> CB = null)\n\
    {\n\
        if(false == IsConnect())    { OnDisconnect();       return; }\n\
        if (0 != ResponseType) { SetCallBack(ResponseType, CB); }\n\
        HNET.NewPacket Packet = new HNET.NewPacket((ushort)@P2@.@P3@);\n\
        SendData.SetPacket(ref Packet);\n\
        Send(Packet);\n\
    }\n\
    /// EndOf @P1@\n\
");
