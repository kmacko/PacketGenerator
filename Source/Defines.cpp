#include "stdafx.h"
#include "Defines.h"


void REPLACE(wstring& Target, const wstring& IDF, const wstring& ReplaceStr, bool Loop)
{
    size_t Pos(Target.find(IDF.c_str()));

    if (Target.npos != Pos)
    {
        Target.replace(Target.begin() + Pos, Target.begin() + Pos + IDF.size(), ReplaceStr.c_str());

        // 없을 때 까지 반복 인 경우 재귀 호출.
        if (true == Loop)
        {
            return REPLACE(Target, IDF, ReplaceStr, Loop);
        }
    }
}

wstring ITOW(int Num)
{
    wchar_t NUM_STR_TEMP[MAX_PATH] = { 0 };

    _itow_s(Num, NUM_STR_TEMP, 10);

    return wstring(NUM_STR_TEMP);
}

wstring GET_TIME()
{
    // 현재 시간.
    time_t Cur(time(nullptr));
    struct tm t;
    localtime_s(&t, &Cur);

    wchar_t Buff[MAX_PATH] = { 0 };
    wsprintf(Buff, L"%4d.%02d.%02d:%02d.%02d.%02d", t.tm_year + 1900,
                                                    t.tm_mon + 1,
                                                    t.tm_mday,
                                                    t.tm_hour,
                                                    t.tm_min,
                                                    t.tm_sec);
    return wstring(Buff);
}

