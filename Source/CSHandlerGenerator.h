#pragma once

class TypeParser;
class TotalParsedData;

class CSHandlerGenerator
{
public :
    CSHandlerGenerator();
    ~CSHandlerGenerator();

public:
    bool                        Initialize(wstring& OutPath, vector<TypeParser*>& vecParser);
    bool                        Generate();

private:
    bool                        CheckProtocols();
    bool                        OpenFiles();
    void                        CloseFiles();
    void                        WriteBase(const wstring& Str);
    void                        WritePartial(const wstring& PartialName, const wstring& Str);

    bool                        GenerateBaseCode();
    bool                        GenerateBasePartialCode(TypeParser* pParser);

    void                        WriteUseingNamespace(vector<wstring>& NamespaceList, wstring PacketFileName = wstring());
    vector<wstring>             MakeNameSpaceList(TotalParsedData* pData);
    EnumData*                   GetMessageTypeData();
    wstring                     GetEncryptBitCodeStr();
    wstring                     GetProtocolNameFromMessageTypeEnum(wstring& EnumData);
    wstring                     GetMessageTypeEnumByProtocolName(wstring& StructName);
    bool                        HasProtocolStructName(TotalParsedData* pData);
    bool                        IsProtoalStructName(wstring& StructName);

private:
    vector<TypeParser*>         _vecParsers;

    vector<wstring>             _vecUsedNamespaces;

    wstring                     _OutPath;

    FILE*                       _pFPBase;
    map<wstring, FILE*>         _mapFPPartials;
};

