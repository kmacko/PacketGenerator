#include "stdafx.h"
#include "CSDefines.h"
#include "CS_SRC_Packet.h"
#include "CS_SRC_Handler.h"
#include "TypeParser.h"
#include "CSGenerator.h"

CSGenerator::CSGenerator()
    : _pParsedData(nullptr),
    _pCSFile(nullptr)
{
}

CSGenerator::~CSGenerator()
{
}

void CSGenerator::Initialize(const wchar_t* pOutPath, TypeParser* pParser)
{
    _OutFilePath = GetOutputPath(pOutPath);

    _PacketFileNameNoExt = pParser->GetPacketFileName();
    _PacketFileName = _PacketFileNameNoExt + PACKET_FILE_EXT;
    _OutFilePath += _PacketFileNameNoExt;

    _pParsedData = pParser->GetParsedData();
    _mapIncludes = pParser->GetIncludesData();      // 컨테이너 복사.
}

bool CSGenerator::GenerateSource()
{
    if (false == OpenFile(_OutFilePath.c_str()))
    {
        return false;
    }

    wstring CurrTime(GET_TIME());

    // 1. 주석.
    wstring Comment(CS_PacketComment);
    REPLACE(Comment, CS_IDF_P1, _PacketFileNameNoExt);
    REPLACE(Comment, CS_IDF_P2, CurrTime);
    REPLACE(Comment, CS_IDF_P3, _PacketFileName);
    WriteCS(Comment);

    // 2. 네임스페이스.
    for (wstring& Namespace : _pParsedData->_NamespaceData)
    {
        wstring NamespaceOut(CS_PacketNamespaceDelcar);
        REPLACE(NamespaceOut, CS_IDF_P1, Namespace);
        WriteCS(NamespaceOut);
    }

    // 3. 열거자 목록.
    if (false == GenerateEnums())
    {
        return false;
    }

    // 열거자 구조체 양쪽 선언이 있으면 엔터 두번.
    if (0 < _pParsedData->_Enums.size() && 0 < _pParsedData->_Structs.size())
    {
        WriteCS(wstring(L"\n\n"));
    }

    // 4. 구조체 목록.
    if (false == GenerateStructs())
    {
        return false;
    }

    // 5. 네임스페이스 닫기.
    for (size_t End(_pParsedData->_NamespaceData.size()) ; 0 < End ; --End)
    {
        wstring NameSpace(_pParsedData->_NamespaceData[End - 1]);
        wstring NamespaceClose(CS_PacketNamespaceClose);
        REPLACE(NamespaceClose, CS_IDF_P1, NameSpace);
        WriteCS(NamespaceClose);
    }

    return true;
}

bool CSGenerator::GenerateEnums()
{
    for (auto& Data : _pParsedData->_Enums)
    {
        wstring EnumStart(CS_PacketEnumStart);
        REPLACE(EnumStart, CS_IDF_P1, Data._Name);
        WriteCS(EnumStart);

        for (int i(0) ; Data._EnumDatas.size() > i ; ++i)
        {
            wstring DataDeclear((INT_MAX == Data._EnumValues[i]) ?
                                CS_PacketEnumDataFormatNonValue :
                                CS_PacketEnumDataFormatValue);
            REPLACE(DataDeclear, CS_IDF_P1, Data._EnumDatas[i]);
            REPLACE(DataDeclear, CS_IDF_P2, ITOW(Data._EnumValues[i]));
            WriteCS(DataDeclear);
        }

        wstring EnumEnd(CS_PacketEnumEnd);
        REPLACE(EnumEnd, CS_IDF_P1, Data._Name);
        WriteCS(EnumEnd);

        // 마지막 열거자가 아니면 엔터 두번.
        if (Data._Name != _pParsedData->_Enums.rbegin()->_Name)
        {
            WriteCS(wstring(L"\n\n"));
        }
    }

    return true;
}

bool CSGenerator::GenerateStructs()
{
    for (auto& Data : _pParsedData->_Structs)
    {
        wstring StructStart(CS_PacketStructStart);
        REPLACE(StructStart, CS_IDF_P1, Data._StructName);
        WriteCS(StructStart);

        // 변수 선언 출력.
        WriteMemberDeclear(&Data);

        if (0 < Data._MemberTypes.size())
        {
            WriteCS(wstring(L"\n\n"));
        }

        // 생성자들.
        wstring Construct1(CS_PacketStructConstruct);
        REPLACE(Construct1, CS_IDF_P1, Data._StructName);
        WriteCS(Construct1);

        // 파라메터 생성자 추가. 멤버가 있을 때만.
        if (0 < Data._MemberTypes.size())
        {
            WriteMemberParamConstructor(&Data);
        }

        WriteSetDataFunc(&Data);
        WriteSetPacketFunc(&Data);

        wstring StructEnd(CS_PacketStructEnd);
        REPLACE(StructEnd, CS_IDF_P1, Data._StructName);
        WriteCS(StructEnd);

        // 마지막 구조체가 아니면 엔터 두번.
        if (Data._StructName != _pParsedData->_Structs.rbegin()->_StructName)
        {
            WriteCS(wstring(L"\n\n"));
        }
    }

    return true;
}

bool CSGenerator::WriteMemberDeclear(StructData* pData)
{
    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring Format(GetMemberDeclearFormat(i, pData));

        wstring ContainerName(GetContainerName(pData->_MemberContainerName[i]));
        wstring TypeName(GetTypeName(pData->_MemberTypes[i]));
        wstring MemberName(pData->_MemberNames[i]);
        wstring FirstEnumName(FirstEnumName(pData->_MemberTypes[i]));
        wstring DefulatValueStr(DefulatValueString(pData->_MemberTypes[i]));
        int ArrayCnt(pData->_MemberArrayCnt[i]);

        REPLACE(Format, CS_IDF_P1, TypeName);
        REPLACE(Format, CS_IDF_P2, MemberName);
        REPLACE(Format, CS_IDF_FIRST_ENUM_DATA, FirstEnumName);
        REPLACE(Format, CS_IDF_DEFAULT_VALUE, DefulatValueStr);
        REPLACE(Format, CS_IDF_ARRAY_CNT, ITOW(ArrayCnt));
        REPLACE(Format, CS_IDF_CONTAINER_TYPE, ContainerName);

        WriteCS(Format);
    }

    return true;
}

// 파라메터 생성자 추가.
bool CSGenerator::WriteMemberParamConstructor(StructData* pData)
{
    wstring Construct(CS_PacketStructConstructWithParam);
    REPLACE(Construct, CS_IDF_P1, pData->_StructName);
    REPLACE(Construct, CS_IDF_P2, GetConstructMemberListStr(pData));
    WriteCS(Construct);

    // 멤버 값 적용.
    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring SetMember((1 < pData->_MemberArrayCnt[i]) ?
                          CS_PacketStructSetMemberValueArray :
                          CS_PacketStructSetMemberValueDefault);
        REPLACE(SetMember, CS_IDF_P1, pData->_MemberNames[i]);
        REPLACE(SetMember, CS_IDF_ARRAY_CNT, ITOW(pData->_MemberArrayCnt[i]));
        WriteCS(SetMember);
    }

    WriteCS(CS_PacketStrcutFuncEnd);

    return true;
}

bool CSGenerator::WriteSetDataFunc(StructData* pData)
{
    wstring SetDataStr(CS_PacketStructSetDataOpen);
    REPLACE(SetDataStr, CS_IDF_P1, pData->_StructName);
    WriteCS(SetDataStr);

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring Format(GetMemberSetDataFormat(i, pData));

        wstring MemberName(pData->_MemberNames[i]);
        wstring TypeName(GetTypeName(pData->_MemberTypes[i]));
        wstring DefualtValueStr(DefulatValueString(pData->_MemberTypes[i]));
        int ArrCnt(pData->_MemberArrayCnt[i]);

        REPLACE(Format, CS_IDF_P1, MemberName);
        REPLACE(Format, CS_IDF_P2, TypeName);
        REPLACE(Format, CS_IDF_ARRAY_CNT, ITOW(ArrCnt));
        REPLACE(Format, CS_IDF_DEFAULT_VALUE, DefualtValueStr);

        WriteCS(Format);
    }

    WriteCS(CS_PacketStrcutFuncEnd);

    return true;
}

bool CSGenerator::WriteSetPacketFunc(StructData* pData)
{
    WriteCS(CS_PacketStructSetPacketOpen);

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring Format(GetMemberSetPacketFormat(i, pData));
        REPLACE(Format, CS_IDF_P1, pData->_MemberNames[i]);
        REPLACE(Format, CS_IDF_ARRAY_CNT, ITOW(pData->_MemberArrayCnt[i]));
        WriteCS(Format);
    }

    WriteCS(CS_PacketStrcutFuncEnd);

    return true;
}

wstring CSGenerator::GetOutputPath(const wchar_t* pOutPath)
{
    wstring FinalPath(pOutPath);
    wchar_t LastChar(FinalPath[FinalPath.size() - 1]);

    // 마지막 글자가 \\, / 가 아니면 \\ 붙인다.
    if (L'\\' != LastChar || L'/' != LastChar)
    {
        FinalPath += L'\\';
    }

    return FinalPath;
}

wstring CSGenerator::GetContainerName(wstring& OrigContainer)
{
    if (0 < OrigContainer.size())
    {
        if (TYPE_LIST == OrigContainer) { return CS_TY_LIST; }
        else
        {
            PRINT(L"오류! 사용 불가능한 컨테이너 자료형 입니다. :" << OrigContainer << endl);
        }
    }

    return wstring(L"");
}

wstring CSGenerator::GetTypeName(wstring& Type)
{
    if (TYPE_I64 == Type)               { return CS_TY_I64; }
    else if (TYPE_I32 == Type)          { return CS_TY_I32; }
    else if (TYPE_I16 == Type)          { return CS_TY_I16; }
    else if (TYPE_I8 == Type)           { return CS_TY_I8; }
    else if (TYPE_f64 == Type)          { return CS_TY_f64; }
    else if (TYPE_f32 == Type)          { return CS_TY_f32; }
    else if (TYPE_UI64 == Type)         { return CS_TY_UI64; }
    else if (TYPE_UI32 == Type)         { return CS_TY_UI32; }
    else if (TYPE_UI16 == Type)         { return CS_TY_UI16; }
    else if (TYPE_STR == Type)          { return CS_TY_STR; }
    else if (TYPE_WSTR == Type)         { return CS_TY_WSTR; }
    else                                { return Type; }        // 구조체, 열거자로 정의된 자료형.
}

wstring CSGenerator::FirstEnumName(wstring& EnumName)
{
    // 열거자 목록에 있는지 조회 한다.
    for (auto& n : _pParsedData->_Enums)
    {
        if (n._Name == EnumName)
        {
            if (0 < n._EnumDatas.size())
            {
                return n._EnumDatas[0];
            }
            else
            {
                PRINT(L"빈 열거자 선언은 에러를 유발 합니다." << EnumName << endl);
                return wstring(L"");
            }
        }
    }

    // 없으면 Include 목록에서 조회.
    for (auto& iter : _mapIncludes)
    {
        for (auto& n : iter.second->_Enums)
        {
            if (0 < n._EnumDatas.size())
            {
                return n._EnumDatas[0];
            }
            else
            {
                PRINT(L"빈 열거자 선언은 에러를 유발 합니다." << EnumName << endl);
                return wstring(L"");
            }
        }
    }
    return wstring(L"");
}

wstring CSGenerator::DefulatValueString(wstring& OrigType)
{
    if (TYPE_I64 == OrigType)           { return CS_TY_I64_DEFAULT_VAL; }
    else if (TYPE_I32 == OrigType)      { return CS_TY_I32_DEFAULT_VAL; }
    else if (TYPE_I16 == OrigType)      { return CS_TY_I16_DEFAULT_VAL; }
    else if (TYPE_I8 == OrigType)       { return CS_TY_I8_DEFAULT_VAL; }
    else if (TYPE_f64 == OrigType)      { return CS_TY_f64_DEFAULT_VAL; }
    else if (TYPE_f32 == OrigType)      { return CS_TY_f32_DEFAULT_VAL; }
    else if (TYPE_UI64 == OrigType)     { return CS_TY_UI64_DEFAULT_VAL; }
    else if (TYPE_UI32 == OrigType)     { return CS_TY_UI32_DEFAULT_VAL; }
    else if (TYPE_UI16 == OrigType)     { return CS_TY_UI16_DEFAULT_VAL; }
    else if (TYPE_STR == OrigType)      { return CS_TY_STR_DEFAULT_VAL; }
    else if (TYPE_WSTR == OrigType)     { return CS_TY_STR_DEFAULT_VAL; }
    else                                { return wstring(L""); }        // 구조체, 열거자로 정의된 자료형.
}

wstring CSGenerator::GetConstructMemberListStr(StructData* pData)
{
    wstring Ret;

    for (size_t i(0) ; pData->_MemberTypes.size() > i ; ++i)
    {
        wstring Format(GetMemberParamListFormat(i, pData));

        wstring ContainerName(GetContainerName(pData->_MemberContainerName[i]));
        wstring TypeName(GetTypeName(pData->_MemberTypes[i]));
        wstring MemberName(pData->_MemberNames[i]);

        REPLACE(Format, CS_IDF_P2, TypeName);
        REPLACE(Format, CS_IDF_P3, MemberName);
        REPLACE(Format, CS_IDF_CONTAINER_TYPE, ContainerName);
        REPLACE(Format, CS_IDF_P1, Ret);
        Ret = Format;

        // 마지막 변수가 아니면 ", " 추가.
        if (i < pData->_MemberTypes.size() - 1)
        {
            Ret += L", ";
        }
    }

    return Ret;
}

bool CSGenerator::IsEnumTypeName(wstring& Type)
{
    // 열거자 목록에 있는지 조회 한다.
    for (auto& n : _pParsedData->_Enums)
    {
        if (n._Name == Type)
        {
            return true;
        }
    }

    // 없으면 Include 목록에서 조회.
    for (auto& iter : _mapIncludes)
    {
        for (auto& n : iter.second->_Enums)
        {
            if (n._Name == Type)
            {
                return true;
            }
        }
    }
    return false;
}

bool CSGenerator::IsDefaultTypeName(wstring& Type)
{
    if (TYPE_I64 == Type    || TYPE_I32 == Type     || TYPE_I16 == Type     ||
        TYPE_UI64 == Type   || TYPE_UI32 == Type    || TYPE_UI16 == Type    ||
        TYPE_I8 == Type     || TYPE_f64 == Type     || TYPE_f32 == Type)
    {
        return true;
    }
    return false;
}

wstring CSGenerator::GetMemberDeclearFormat(size_t MemberIndex, StructData* pData)
{
    // 배열.
    if (1 < pData->_MemberArrayCnt[MemberIndex])
    {
        return CS_PacketStructMemberInitArray;
    }
    // 컨테이너.
    else if (0 < pData->_MemberContainerName[MemberIndex].size())
    {
        // List 현재 하나만 지원함.
        if (TYPE_LIST == pData->_MemberContainerName[MemberIndex])
        {
            return CS_PacketStructMemberInitList;
        }
        else
        {
            PRINT(L"사용할 수 없는 구조체 이름 입니다" << pData->_MemberContainerName[MemberIndex] << endl);
            return wstring(L"");
        }
    }
    // 기본 자료형.
    else if (true == IsDefaultTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructMemberInitDefault;
    }
    else if (TYPE_WSTR == pData->_MemberTypes[MemberIndex] ||
             TYPE_STR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructMemberInitDefault;
    }
    // 열거자.
    else if (true == IsEnumTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructMemberInitEnum;
    }

    // 나머지 구조체.
    return CS_PacketStructMemberInitCustom;
}

wstring CSGenerator::GetMemberParamListFormat(size_t MemberIndex, StructData* pData)
{
    // 배열.
    if (1 < pData->_MemberArrayCnt[MemberIndex])
    {
        return CS_PacketStructMemberParamFormatArray;
    }
    // 컨테이너.
    else if (0 < pData->_MemberContainerName[MemberIndex].size())
    {
        // List 현재 하나만 지원함.
        if (TYPE_LIST == pData->_MemberContainerName[MemberIndex])
        {
            return CS_PacketStructMemberParamFormatList;
        }
        else
        {
            PRINT(L"사용할 수 없는 구조체 이름 입니다" << pData->_MemberContainerName[MemberIndex] << endl);
            return wstring(L"");
        }
    }
    // 기본 자료형, 문자열들, 열거자, 구조체.
    return CS_PacketStructMemberParamFormatDefault;
}

wstring CSGenerator::GetMemberSetDataFormat(size_t MemberIndex, StructData* pData)
{
    // 배열.
    if (1 < pData->_MemberArrayCnt[MemberIndex])
    {
        return CS_PacketStructSetDataMemberArray;
    }
    // 컨테이너.
    else if (0 < pData->_MemberContainerName[MemberIndex].size())
    {
        // List 현재 하나만 지원함.
        if (TYPE_LIST == pData->_MemberContainerName[MemberIndex])
        {
            // 리스트내 자료형에 따라 변수 추가 방법이 다름.
            return GetMemberSetDataContainerListFormat(MemberIndex, pData);
        }
        else
        {
            PRINT(L"사용할 수 없는 구조체 이름 입니다" << pData->_MemberContainerName[MemberIndex] << endl);
            return wstring(L"");
        }
    }
    // string
    else if (TYPE_STR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetDataMemberString;
    }
    // wstring
    else if (TYPE_WSTR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetDataMemberWString;
    }
    // 기본자료형 - 디폴트.
    else if (true == IsDefaultTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetDataMemberDefault;
    }
    // 열거자.
    else if (true == IsEnumTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetDataMemberEnum;
    }
    // 나머지 구조체.
    return CS_PacketStructSetDataMemberCustom;
}

wstring CSGenerator::GetMemberSetDataContainerListFormat(size_t MemberIndex, StructData* pData)
{
    // 컨테이너에 대한 내용이므로 배열, 컨테이너 조회 없음.
    // string
    if (TYPE_STR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetDataMemberListString;
    }
    // wstring
    else if (TYPE_WSTR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetDataMemberListWString;
    }
    // 기본자료형 - 디폴트.
    else if (true == IsDefaultTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetDataMemberListDefault;
    }
    // 열거자.
    else if (true == IsEnumTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetDataMemberListEnum;
    }
    // 나머지 구조체.
    return CS_PacketStructSetDataMemberListCustom;
}

wstring CSGenerator::GetMemberSetPacketContainerListFormat(size_t MemberIndex, StructData* pData)
{
    // 컨테이너에 대한 내용이므로 배열, 컨테이너 조회 없음.
    // string
    if (TYPE_STR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetPacketMemberListString;
    }
    // wstring
    else if (TYPE_WSTR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetPacketMemberListWString;
    }
    // 기본자료형 - 디폴트.
    else if (true == IsDefaultTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetPacketMemberListDefafult;
    }
    // 열거자.
    else if (true == IsEnumTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetPacketMemberListEnum;
    }
    // 나머지 구조체.
    return CS_PacketStructSetPacketMemberListCustom;
}

wstring CSGenerator::GetMemberSetPacketFormat(size_t MemberIndex, StructData* pData)
{
    // 배열.
    if (1 < pData->_MemberArrayCnt[MemberIndex])
    {
        return CS_PacketStructSetPacketMemberArray;
    }
    // 컨테이너.
    else if (0 < pData->_MemberContainerName[MemberIndex].size())
    {
        // List 현재 하나만 지원함.
        if (TYPE_LIST == pData->_MemberContainerName[MemberIndex])
        {
            // 리스트내 자료형에 따라 변수 추가 방법이 다름.
            wstring ListInFormat(GetMemberSetPacketContainerListFormat(MemberIndex, pData));
            wstring Ret(CS_PacketStructSetPacketMemberList);
            REPLACE(Ret, CS_IDF_P2, ListInFormat);
            return Ret;
        }
        else
        {
            PRINT(L"사용할 수 없는 구조체 이름 입니다" << pData->_MemberContainerName[MemberIndex] << endl);
            return wstring(L"");
        }
    }
    // string
    else if (TYPE_STR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetPacketMemberString;
    }
    // wstring
    else if (TYPE_WSTR == pData->_MemberTypes[MemberIndex])
    {
        return CS_PacketStructSetPacketMemberWString;
    }
    // 기본자료형 - 디폴트.
    else if (true == IsDefaultTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetPacketMemberDefault;
    }
    // 열거자.
    else if (true == IsEnumTypeName(pData->_MemberTypes[MemberIndex]))
    {
        return CS_PacketStructSetPacketMemberEnum;
    }
    // 나머지 구조체.
    return CS_PacketStructSetPacketMemberCustom;
}

bool CSGenerator::OpenFile(const wchar_t* pOutPath)
{
    wstring CSFile(pOutPath);
    CSFile += OUT_EXT_CS;

    auto* cpCSFile(WC2C(CSFile.c_str()));
    if (0 != fopen_s(&_pCSFile, cpCSFile, "w+"))
    {
        PRINT(L"CS 파일 쓰기모드 열기 실패! - " << CSFile << endl);
        return false;
    }

    return true;
}

void CSGenerator::FileClose()
{
    if (nullptr != _pCSFile)
    {
        fclose(_pCSFile);
        _pCSFile = nullptr;
    }
}

void CSGenerator::WriteCS(const wstring& Str)
{
    string Temp(WC2C(Str.c_str()));
    fwrite(Temp.c_str(), sizeof(char), Temp.size(), _pCSFile);
}

