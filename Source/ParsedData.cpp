#include "stdafx.h"
#include "Defines.h"
#include "ParsedData.h"

NamespaceData::NamespaceData(wstring& Name)
    : _Name(Name)
{
}

NamespaceData::~NamespaceData()
{
}


EnumData::EnumData()
{
}

EnumData::EnumData(wstring& EnumName)
    : _Name(EnumName)
{
}

EnumData::~EnumData()
{
}


StructData::StructData() : _IsEncrypt(false)
{
}

StructData::StructData(wstring& StructName)
    : _StructName(StructName),
      _IsEncrypt(false)
{
}

StructData::~StructData()
{
}


TotalParsedData::TotalParsedData()
{
}

TotalParsedData::~TotalParsedData()
{
}

void TotalParsedData::AddNamespaceData(wstring& NamespaceName)
{
    _NamespaceData.push_back(NamespaceName);
}

void TotalParsedData::AddEnumName(wstring& EnumName)
{
    _Enums.push_back(EnumData(EnumName));
}

void TotalParsedData::AddEnumData(wstring& DataName, int DataValue)
{
    if (0 >= _Enums.size())
    {
        PRINT(L"내부에러! 열거자 선언 없이 열거자 정보 접근 발생! 디버깅 필요.");
        return;
    }

    EnumData& RefData(_Enums[_Enums.size() - 1]);

    RefData._EnumDatas.push_back(DataName);
    RefData._EnumValues.push_back(DataValue);
}

void TotalParsedData::AddNewStruct(wstring& StructName)
{
    _Structs.push_back(StructData(StructName));
}

void TotalParsedData::AddStructMember(wstring& TypeName, wstring& MemberName, int ArrayCnt, wstring& ContainerName)
{
    if (0 >= _Structs.size())
    {
        PRINT(L"내부에러! 구조체 선언 없이 멤버 추가 발생! 디버깅 필요.");
        return;
    }

    auto& RefData(_Structs[_Structs.size() - 1]);
    RefData._MemberTypes.push_back(TypeName);
    RefData._MemberNames.push_back(MemberName);
    RefData._MemberArrayCnt.push_back(ArrayCnt);
    RefData._MemberContainerName.push_back(ContainerName);
}

void TotalParsedData::SetStructEncrypt()
{
    if (0 >= _Structs.size())
    {
        PRINT(L"내부에러! 구조체 선언 없이 암호화 설정 호출됨! 디버깅 필요.");
        return;
    }

    auto& RefData(_Structs[_Structs.size() - 1]);
    RefData._IsEncrypt = true;
}

bool TotalParsedData::IsExistStructName(const wstring& StructName)
{
    for (auto& Data : _Structs)
    {
        if (StructName == Data._StructName)
        {
            return true;
        }
    }

    return false;
}

