#pragma once

#include <string>
using namespace std;

//////////////////////////////////////////////////////////////////////
// 패킷 정의 .Cpp
const wstring CPPS_Header(L"\
//========================================================\n\
// !PACKET_NAME!.cpp\n\
// !YYYYMMDD! By PacketGenerator.exe 자동 생성된 코드.\n\
// Generated From !PACKET_FILE_NAME!\n\
//========================================================\n\
#include \"stdafx.h\"\n\
#include \"HNetwork/HNetwork.h\"\n\
#include \"!PACKET_NAME!.h\"\n\
\n\
");


// 구조체.
const wstring CPPS_OperatorOpen(L"\
!STRUCT_NAME!& !STRUCT_NAME!::operator=(!STRUCT_NAME!& Ref)\n\
{\n\
");

const wstring CPPS_OperatorMember(L"    #MEMBER_NAME# = Ref.#MEMBER_NAME#;\n");
const wstring CPPS_OperatorMemberArray(L"\
    for (int i(0) ; #STRUCT_MEMBER_ARRAY_CNT# > i ; ++i)\n\
    {\n\
        #MEMBER_NAME#[i] = Ref.#MEMBER_NAME#[i];\n\
    }\n\
");

const wstring CPPS_OperatorClose(L"\
    return *this;\n\
}\n\
");

const wstring CPPS_Constructor1(L"\
!STRUCT_NAME!::!STRUCT_NAME!()\n\
{\n\
    // 기본 생성자. 멤버 초기화 없음. 직접 채워서 사용 할것.\n\
}\n\
");

const wstring CPPS_Constructor2(L"\
!STRUCT_NAME!::!STRUCT_NAME!(%STRUCT_CONSTRUCT_MEMBER_LIST%)\n\
{\n\
");

const wstring CPPS_Constructor3(L"\
!STRUCT_NAME!::!STRUCT_NAME!(const HNetPacket& Packet)\n\
{\n\
    SetData(Packet);\n\
}\n\
");

const wstring CPPS_SetData(L"\
void !STRUCT_NAME!::SetData(const HNetPacket& Packet)\n\
{\n\
");

const wstring CPPS_SetDataMemberDefault(L"    Packet.Out(#MEMBER_NAME#);\n");
const wstring CPPS_SetDataMemberCustom(L"    #MEMBER_NAME#.SetData(Packet);\n");
const wstring CPPS_SetDataMemberWstr(L"\
    wchar_t* #MEMBER_NAME#_(nullptr);\n\
    Packet.Out(#MEMBER_NAME#_);\n\
    if(nullptr != #MEMBER_NAME#_) { #MEMBER_NAME# = #MEMBER_NAME#_; }\n\
");
const wstring CPPS_SetDataMemberStr(L"\
    char* #MEMBER_NAME#_(nullptr);\n\
    Packet.Out(#MEMBER_NAME#_);\n\
    if(nullptr != #MEMBER_NAME#_) { #MEMBER_NAME# = #MEMBER_NAME#_; }\n\
");
const wstring CPPS_SetDataMemberEnum(L"\
    short #MEMBER_NAME#_(0);\n\
    Packet.Out(#MEMBER_NAME#_);\n\
    #MEMBER_NAME# = (!ENUM_NAME!)#MEMBER_NAME#_;\n\
");
const wstring CPPS_SetDataMemberArray(L"\
    for (int i(0) ; #STRUCT_MEMBER_ARRAY_CNT# > i ; ++i)\n\
    {\n\
        Packet.Out(#MEMBER_NAME#[i]);\n\
    }\n\
");

const wstring CPPS_SetDataMemberListDefault(L"\
    USHORT #MEMBER_NAME#Cnt(0);\n\
    Packet.Out(#MEMBER_NAME#Cnt);\n\
    for (USHORT i(0) ; #MEMBER_NAME#Cnt > i ; ++i)\n\
    {\n\
        #MEMBER_TYPE_IN_ARRAY# Temp;\n\
        Packet.Out(Temp);\n\
        #MEMBER_NAME#.push_back(Temp);\n\
    }\n\
");

const wstring CPPS_SetDataMemberListCustom(L"\
    USHORT #MEMBER_NAME#Cnt(0);\n\
    Packet.Out(#MEMBER_NAME#Cnt);\n\
    for (USHORT i(0) ; #MEMBER_NAME#Cnt > i ; ++i)\n\
    {\n\
        #MEMBER_NAME#.push_back(#MEMBER_TYPE_IN_ARRAY#(Packet));\n\
    }\n\
");
const wstring CPPS_SetDataMemberListWStr(L"\
    USHORT #MEMBER_NAME#Cnt(0);\n\
    Packet.Out(#MEMBER_NAME#Cnt);\n\
    for (USHORT i(0) ; #MEMBER_NAME#Cnt > i ; ++i)\n\
    {\n\
        wchar_t* pTemp;\n\
        Packet.Out(pTemp);\n\
        #MEMBER_NAME#.push_back(pTemp);\n\
    }\n\
");
const wstring CPPS_SetDataMemberListStr(L"\
    USHORT #MEMBER_NAME#Cnt(0);\n\
    Packet.Out(#MEMBER_NAME#Cnt);\n\
    for (USHORT i(0) ; #MEMBER_NAME#Cnt > i ; ++i)\n\
    {\n\
        char* pTemp;\n\
        Packet.Out(pTemp);\n\
        #MEMBER_NAME#.push_back(pTemp);\n\
    }\n\
");
const wstring CPPS_SetDataMemberListEnum(L"\
    USHORT #MEMBER_NAME#Cnt(0);\n\
    Packet.Out(#MEMBER_NAME#Cnt);\n\
    for(USHORT i(0) ; #MEMBER_NAME#Cnt > i ; ++i)\n\
    {\n\
        short temp(0);\n\
        Packet.Out(temp); \n\
        #MEMBER_NAME#.push_back((!ENUM_NAME!)temp);\n\
    }\n\
");

const wstring CPPS_SetPacket(L"\
void !STRUCT_NAME!::SetPacket(HNET::NewPacket& pPacket)\n\
{\n\
");

const wstring CPPS_SetPacketMemberDefault(L"    pPacket->In(#MEMBER_NAME#);\n");
const wstring CPPS_SetPacketMemberCustom(L"    #MEMBER_NAME#.SetPacket(pPacket);\n");
const wstring CPPS_SetPacketMemberWStrAndStr(L"    pPacket->In(#MEMBER_NAME#.c_str());\n");
const wstring CPPS_SetPacketMemberEnum(L"    pPacket->In((short)#MEMBER_NAME#);\n");
const wstring CPPS_SetPacketMemberArray(L"\
    for (int i(0) ; #STRUCT_MEMBER_ARRAY_CNT# > i ; ++i)\n\
    {\n\
        pPacket->In(#MEMBER_NAME#[i]);\n\
    }\n\
");
const wstring CPPS_SetPacketMemberListDefault(L"\
    pPacket->In((USHORT)#MEMBER_NAME#.size());\n\
    for (auto& Data : #MEMBER_NAME#)\n\
    {\n\
        pPacket->In(Data);\n\
    }\n\
");
const wstring CPPS_SetPacketMemberListCustom(L"\
    pPacket->In((USHORT)#MEMBER_NAME#.size());\n\
    for (auto& Data : #MEMBER_NAME#)\n\
    {\n\
        Data.SetPacket(pPacket);\n\
    }\n\
");
const wstring CPPS_SetPacketMemberListWStrAndStr(L"\
    pPacket->In((USHORT)#MEMBER_NAME#.size());\n\
    for (auto& Data : #MEMBER_NAME#)\n\
    {\n\
        pPacket->In(Data.c_str()); \n\
    }\n\
");
const wstring CPPS_SetPacketMemberListEnum(L"\
    pPacket->In((USHORT)#MEMBER_NAME#.size());\n\
    for (auto& Data : #MEMBER_NAME#)\n\
    {\n\
        short temp((short)Data);\n\
        pPacket->In(temp);\n\
    }\n\
");

const wstring CPPS_FunctionClose(L"}\n");

// 구조체 배열 맴버 초기화 코드.
const wstring CPPS_MemberInit(L"\
    #MEMBER_NAME# = #MEMBER_NAME#_;\n\
");
const wstring CPPS_MemberArrayInit(L"\
    for (int i(0) ; #STRUCT_MEMBER_ARRAY_CNT# > i ; ++i)\n\
    {\n\
        #MEMBER_NAME#[i] = #MEMBER_NAME#_[i];\n\
    }\n\
");

// 열거자 멤버 문자열 정의.
const wstring CPPS_EnumStrDeclear(L"\
map<int, wstring> g_Map!ENUM_NAME!Str = \n\
{\n\
");

const wstring CPPS_EnumStrDeclearData(L"\
    { (int)#ENUM_DATA_NAME#, L\"#ENUM_DATA_NAME#\" },\n\
");

const wstring CPPS_EnumStrDeclearEnd(L"\
}; // g_Map!ENUM_NAME!Str\n\
");


//////////////////////////////////////////////////////////////////////
// 패킷 핸들러 .Cpp
const wstring CPPS_HandlCppCommentClassOpen(L"\
//========================================================\n\
// PacketHandler.cpp\n\
// !YYYYMMDD! By PacketGenerator.exe 자동 생성된 코드.\n\
// 자동생성된 패킷들을 제어 처리 하는 클래스 정의.\n\
//========================================================\n\
#include \"stdafx.h\"\n\
#include \"PacketHandler.h\"\n\
\n\
");

const wstring CPPS_HandlCppConstructorDefaultFunc(L"\
\n\
PacketHandler::PacketHandler()\n\
{\n\
}\n\
\n\
PacketHandler::~PacketHandler()\n\
{\n\
}\n\
\n\
void PacketHandler::UnDeclearMessageReceived(MessageType Type)\n\
{\n\
    LOG_WARNING(L\"정의하지 않은 패킷 수신 발생함. MessageType:%s (%d)\", g_MapMessageTypeStr[Type].c_str(), Type);\n\
}\n\
\n\
void PacketHandler::UnKnownMessageType(USHORT Type)\n\
{\n\
    LOG_ERROR(L\"알수없는 메세지 타입. PacketType:%d\", Type); \n\
}\n\
");

const wstring CPPS_HandlCppOnMessageOpen(L"\
\n\
void PacketHandler::OnMessage(NetId netId, const HNetPacket& Packet)\n\
{\n\
    switch (Packet.Type())\n\
    {\n\
");

const wstring CPPS_HandlCppOnMessageContinue(L"    case @P1@: { OnRecv@P2@(netId, Packet); } break;\n");

const wstring CPPS_HandlCppOnMessageClose(L"\
    default: { UnKnownMessageType(Packet.Type()); } break;\n\
    }\n\
");

const wstring CPPS_HandlCppEncrypt_Open(L"\
\n\
void PacketHandler::OnEncrypt(USHORT type, char* buf, USHORT lenth)\n\
{\n\
    switch (type)\n\
    {\n\
");

const wstring CPPS_HandlCppDecrypt_Open(L"\
void PacketHandler::OnDecrypt(USHORT type, char* buf, USHORT lenth)\n\
{\n\
    switch (type)\n\
    {\n\
");

// @P1@ : 암호화된 구조체 이름.
const wstring CPPS_HandlCppEncryptDecrypt_Case(L"\
    case k@P1@:\n\
");

// @P1@ : 암호화시 사용할 비트 연산 값.
const wstring CPPS_HandlCppEncryptDecrypt_Close(L"\
        break;\n\
    default: { return; }\n\
    }\n\
\n\
    for (USHORT i = 0 ; lenth > i ; ++i)\n\
    {\n\
        buf[i] ^= @P1@;\n\
    }\n\
}\n\
\n\
");
