#pragma once

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cctype>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#include <errno.h>
#include <windows.h>
#include <time.h>

using namespace std;

#include "Defines.h"
#include "ParserDefines.h"
#include "ParsedData.h"


extern char* WC2C(const wchar_t* str);

extern void TOKEN(const wchar_t* In, const wchar_t Delim, std::vector<std::wstring> &Out);

extern void PRINTF(const wchar_t* Msg);
extern void PRINTF(const wstringstream& Msg);
extern void PRINTF(const wstring& Msg);


// 대소 문자 구분 안하는 경우 CheckAnyCase 에 true 넘길것.
extern bool g_ContainsStr(const wstring& Target, const wchar_t* Find, bool AnyCase = false);
extern void g_RemoveStr(wstring& RefStr, const wchar_t* Find, bool AnyCase = false);


#define PRINT(x)                wstringstream Stream;\
                                Stream << x;\
                                PRINTF(Stream.str())

#define SAFE_DELETE(x)          if (nullptr != x) { delete x;   x = nullptr; }

#define SAFE_DELETE_MAP(x)      for (auto Iter : x)\
                                {\
                                    SAFE_DELETE(Iter.second);\
                                }\
                                x.clear()

#define SAFE_DELETE_VEC(x)      for (auto* pData : x)\
                                {\
                                    SAFE_DELETE(pData);\
                                }\
                                x.clear()

