#pragma once

#include <string>
using namespace std;

// 패킷정의파일, 날짜, 패킷정의파일+확장자.
const wstring CS_PacketComment(L"\
///////////////////////////////////////////////////////////////\n\
/// @P1@.cs\n\
/// @P2@ PacketGenerator.exe 에 의해\n\
/// @P3@ 파일로 부터 자동 생성됨. \n\
//////////////////////////////////////////////////////////////\n\
\n\
");

const wstring CS_PacketNamespaceDelcar(L"\
namespace @P1@\n\
{\n\
");

const wstring CS_PacketNamespaceClose(L"\
}; // namespace @P1@\n\
");


// 열거자.
const wstring CS_PacketEnumStart(L"\
public enum @P1@\n\
{\n\
");

const wstring CS_PacketEnumDataFormatNonValue(L"    @P1@,\n");
const wstring CS_PacketEnumDataFormatValue(L"    @P1@ = @P2@,\n");

const wstring CS_PacketEnumEnd(L"\
}; // enum @P1@\n\
");

// 구조체.
const wstring CS_PacketStructStart(L"\
public class @P1@\n\
{\n\
");

// 기본 + string + wstring: 자료형@P1@, 변수명@P2@, 기본값.
const wstring CS_PacketStructMemberInitDefault(L"\
    public @P1@ @P2@ = #DEFAULT_VALUE#;\n\
");
// 배열: 자료형@P1@, 변수명@P2@, 배열수.
const wstring CS_PacketStructMemberInitArray(L"\
    public @P1@[] @P2@ = new @P1@[#ARRAY_CNT#];\n\
");
// List: 컨테이너 자료형, 자료형@P1@, 변수명@P2@.
const wstring CS_PacketStructMemberInitList(L"\
    public #CONTAINER_TYPE#<@P1@> @P2@ = new #CONTAINER_TYPE#<@P1@>();\n\
");
// 구조체: 구조체명@P1@, 변수명@P2@.
const wstring CS_PacketStructMemberInitCustom(L"\
    public @P1@ @P2@ = new @P1@();\n\
");
// 열거자: 열거자명@P1@, 변수명@P2@, 첫열거자값.
const wstring CS_PacketStructMemberInitEnum(L"\
    public @P1@ @P2@ = @P1@.#FIRST_ENUM_DATA#;\n\
");

////////////////////// 생성자.
const wstring CS_PacketStructConstruct(L"\
    public @P1@()\n\
    {\n\
    }\n\
");

// 구조체이름@P1@, 파라메터목록@P2@
const wstring CS_PacketStructConstructWithParam(L"\
\n\
    public @P1@(@P2@)\n\
    {\n\
");

// 기본,문자열,구조체,열거자: 이전리스트@P1@ 자료형@P2@, 변수명@P3@.
const wstring CS_PacketStructMemberParamFormatDefault(L"@P1@@P2@ @P3@_");
// 배열: 이전리스트@P1@ 자료형@P2@, 변수명@P3@
const wstring CS_PacketStructMemberParamFormatArray(L"@P1@@P2@[] @P3@_");
// List: 이전리스트@P1@ 컨테이너 자료형, 자료형@P2@, 변수명@P3@.
const wstring CS_PacketStructMemberParamFormatList(L"@P1@#CONTAINER_TYPE#<@P2@> @P3@_");

// 배열을 제외한 모든 자료형은 대입으로 끝남.
const wstring CS_PacketStructSetMemberValueDefault(L"        @P1@ = @P1@_;\n");
const wstring CS_PacketStructSetMemberValueArray(L"\
        for (int i = 0 ; #ARRAY_CNT# > i ; ++i)\n\
        {\n\
            @P1@[i] = @P1@_[i];\n\
        }\n\
");

/////////// SetPacket 처리.
const wstring CS_PacketStructSetDataOpen(L"\
\n\
    public @P1@(HNetPacket Packet)\n\
    {\n\
        SetData(Packet);\n\
    }\n\
\n\
    public void SetData(HNetPacket Packet)\n\
    {\n\
");

// 기본자료형 : 변수명@P1@
const wstring CS_PacketStructSetDataMemberDefault(L"        Packet.Out(ref @P1@);\n");
// string : 변수명@P1@
const wstring CS_PacketStructSetDataMemberString(L"\
        Packet.Out(ref @P1@, System.Text.Encoding.UTF8);\n");
// wstring : 변수명@P1@
const wstring CS_PacketStructSetDataMemberWString(L"\
        Packet.Out(ref @P1@, System.Text.Encoding.Unicode);\n");
// 열거자 : 변수명@P1@, 자료형@P3@
const wstring CS_PacketStructSetDataMemberEnum(L"\
        short @P1@_ = 0;\n\
        Packet.Out(ref @P1@_);\n\
        @P1@ = (@P2@)@P1@_;\n\
");
// 구조체 : 변수명@P1@
const wstring CS_PacketStructSetDataMemberCustom(L"        @P1@.SetData(Packet);\n");
// 배열 : 변수명@P1@, 배열갯수
const wstring CS_PacketStructSetDataMemberArray(L"\
        for (int i = 0 ; #ARRAY_CNT# > i ; ++i)\n\
        {\n\
            Packet.Out(ref @P1@[i]);\n\
        }\n\
");
// List : 변수명@P1@, 자료형@P2@
const wstring CS_PacketStructSetDataMemberListDefault(L"\
        @P1@.Clear();\n\
        short @P1@Cnt = 0;\n\
        Packet.Out(ref @P1@Cnt);\n\
        for (short i = 0 ; @P1@Cnt > i ; ++i)\n\
        {\n\
            @P2@ temp = #DEFAULT_VALUE#;\n\
            Packet.Out(ref temp);\n\
            @P1@.Add(temp);\n\
        }\n\
");
// 열거자 : 변수명@P1@, 자료형@P2@
const wstring CS_PacketStructSetDataMemberListEnum(L"\
        @P1@.Clear();\n\
        short @P1@Cnt = 0;\n\
        Packet.Out(ref @P1@Cnt);\n\
        for (short i = 0 ; @P1@Cnt > i ; ++i)\n\
        {\n\
            short temp = 0;\n\
            Packet.Out(ref temp);\n\
            @P1@.Add((@P3@)temp);\n\
        }\n\
");
// string : 변수명@P1@
const wstring CS_PacketStructSetDataMemberListString(L"\
        @P1@.Clear();\n\
        short @P1@Cnt = 0;\n\
        Packet.Out(ref @P1@Cnt);\n\
        for (short i = 0 ; @P1@Cnt > i ; ++i)\n\
        {\n\
            string temp = \"\";\n\
            Packet.Out(ref temp, System.Text.Encoding.UTF8);\n\
            @P1@.Add(temp);\n\
        }\n\
");
// wstring : 변수명@P1@
const wstring CS_PacketStructSetDataMemberListWString(L"\
        @P1@.Clear();\n\
        short @P1@Cnt = 0;\n\
        Packet.Out(ref @P1@Cnt);\n\
        for (short i = 0 ; @P1@Cnt > i ; ++i)\n\
        {\n\
            string temp = \"\";\n\
            Packet.Out(ref temp, System.Text.Encoding.Unicode);\n\
            @P1@.Add(temp);\n\
        }\n\
");
// 구조체 : 변수명@P1@, 자료형@P2@
const wstring CS_PacketStructSetDataMemberListCustom(L"\
        @P1@.Clear();\n\
        short @P1@Cnt = 0;\n\
        Packet.Out(ref @P1@Cnt);\n\
        for (short i = 0 ; @P1@Cnt > i ; ++i)\n\
        {\n\
            @P2@ temp = new @P2@();\n\
            temp.SetData(Packet);\n\
            @P1@.Add(temp);\n\
        }\n\
");

//////////////// SetPacket
const wstring CS_PacketStructSetPacketOpen(L"\
\n\
    public void SetPacket(ref HNET.NewPacket Packet)\n\
    {\n\
");

// 열거자, 기본자료형 : 변수명@P1@
const wstring CS_PacketStructSetPacketMemberDefault(L"        Packet.In(@P1@);\n");
// string : 변수명@P1@
const wstring CS_PacketStructSetPacketMemberString(L"        Packet.In(@P1@, System.Text.Encoding.UTF8);\n");
// wstring : 변수명@P1@
const wstring CS_PacketStructSetPacketMemberWString(L"        Packet.In(@P1@, System.Text.Encoding.Unicode);\n");
const wstring CS_PacketStructSetPacketMemberCustom(L"        @P1@.SetPacket(ref Packet);\n");
const wstring CS_PacketStructSetPacketMemberEnum(L"        Packet.In((short)@P1@);\n");
// 배열 : 변수명@P1@, 배열갯수
const wstring CS_PacketStructSetPacketMemberArray(L"\
        for (int i = 0 ; #ARRAY_CNT# > i ; ++i)\n\
        {\n\
            Packet.In(@P1@[i]);\n\
        }\n\
");
// List : 변수명@P1@, 자료형별SetPacket포멧@P2@
const wstring CS_PacketStructSetPacketMemberList(L"\
        Packet.In((short)@P1@.Count);\n\
        foreach (var temp in @P1@)\n\
        {\n\
@P2@\
        }\n\
");

// 기본자료형 :
const wstring CS_PacketStructSetPacketMemberListDefafult(L"            Packet.In(temp);\n");
// 열거자 :
const wstring CS_PacketStructSetPacketMemberListEnum(L"            Packet.In((short)temp);\n");
// string :
const wstring CS_PacketStructSetPacketMemberListString(L"            Packet.In(temp, System.Text.Encoding.UTF8);\n");
// wstring :
const wstring CS_PacketStructSetPacketMemberListWString(L"            Packet.In(temp, System.Text.Encoding.Unicode);\n");
// 구조체.
const wstring CS_PacketStructSetPacketMemberListCustom(L"            temp.SetPacket(ref Packet);\n");

const wstring CS_PacketStrcutFuncEnd(L"    }\n");

const wstring CS_PacketStructEnd(L"\
}; // class @P1@\n\
");
