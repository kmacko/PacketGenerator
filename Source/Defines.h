#pragma once

#define PACKET_FILE_EXT         L".packet"
#define OPT_GEN                 L"-gen"
#define OPT_HANDLE              L"-handler"

#define CSHARP_GEN_NAME         L"cs"
#define CPP_GEN_NAME            L"cpp"
#define C_RET                   L'\n'
#define S_RET                   L"\n"

#define TOLOWER(x)              std::transform(x.cbegin(), x.cend(), x.begin(), std::tolower)
#define TOUPPER(x)              std::transform(x.cbegin(), x.cend(), x.begin(), std::toupper)

#define LINE_BUFF_SIZE          8192


#define LENGTH(x)               std::wcslen(x)


extern void REPLACE(wstring& Target, const wstring& IDF, const wstring& ReplaceStr, bool Loop = true);

extern wstring ITOW(int Num);

extern wstring GET_TIME();