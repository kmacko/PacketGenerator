﻿// PacketGenerator.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "stdafx.h"
#include "Defines.h"
#include "TypeParser.h"
#include "CSGenerator.h"
#include "CppGenerator.h"
#include "CppHandlerGenerator.h"
#include "CSHandlerGenerator.h"


void g_ParamError(const wchar_t* pReason);
void g_RunWithPacketGen(const wchar_t* pCodeType, wchar_t** argv);
void g_RunWithHandleGen(const wchar_t* CodeType, int argc, wchar_t** argv);


int wmain(int argc, wchar_t* argv[])
{
    if (5 > argc)
    {
        g_ParamError(L"파라메터 부족! Readme.txt 참고 해주세요.");
        return -1;
    }

    wstring Option(argv[1]);
    TOLOWER(Option);
    if (OPT_GEN != Option && OPT_HANDLE != Option)
    {
        g_ParamError(L"알수 없는 옵션 입니다! Readme.txt 참고 해주세요");
        return -2;
    }

    // 코드 생성 타입.
    wstring CodeType(argv[2]);
    TOLOWER(CodeType);
    if (CSHARP_GEN_NAME != CodeType && CPP_GEN_NAME != CodeType)
    {
        g_ParamError(L"불가능한 언어타입!");
        return -3;
    }

    if (OPT_GEN == Option)
    {
        g_RunWithPacketGen(CodeType.c_str(), argv);
    }
    else if(OPT_HANDLE == Option)
    {
        g_RunWithHandleGen(CodeType.c_str(), argc, argv);
    }

    return 0;
}

void g_ParamError(const wchar_t* pReason)
{
    PRINT(pReason << L" Ex)PacketGenerator.exe -gen cs(cpp) 패킷.packet 경로" << std::endl);
}

void g_RunWithPacketGen(const wchar_t* pCodeType, wchar_t** argv)
{
    wstring CodeType(pCodeType);

    // 입력 파일 있는지 체크.
    wstring InputFile(argv[3]);
    if (0 != _waccess_s(InputFile.c_str(), 0))
    {
        g_ParamError(L"패킷파일 경로 확인!");
        return;
    }

    // 출력 경로 체크.
    wstring OutPath(argv[4]);
    errno_t OutR(_waccess_s(OutPath.c_str(), 0));

    if (EACCES == OutR)
    {
        g_ParamError(L"출력 위치에 엑세스 권한없음!");
        return;
    }
    else if (ENOENT == OutR)
    {
        g_ParamError(L"출력 경로를 찾을 수 없음!");
        return;
    }

    TypeParser Parser;
    if (false == Parser.Parse(InputFile.c_str()))
    {
        PRINT(L"입력파일 파싱 실패!");
        return;
    }

    if (CPP_GEN_NAME == CodeType)
    {
        CppGenerator CppGen;
        CppGen.Initialize(OutPath.c_str(), &Parser);
        if (false == CppGen.GenerateSource())
        {
            PRINT(L"C++ 코드 생성 실패 했습니다!");
            return;
        }
    }
    else if (CSHARP_GEN_NAME == CodeType)
    {
        CSGenerator CSGen;
        CSGen.Initialize(OutPath.c_str(), &Parser);
        if (false == CSGen.GenerateSource())
        {
            PRINT(L"C# 코드 생성 실패 했습니다!");
            return;
        }
    }

    // 파서 메모리 해제.
    Parser.Release();
}

void g_RunWithHandleGen(const wchar_t* pCodeType, int argc, wchar_t** argv)
{
    wstring CodeType(pCodeType);

    // 출력 경로 체크.
    wstring OutPath(argv[3]);
    errno_t OutR(_waccess_s(OutPath.c_str(), 0));

    if (EACCES == OutR)
    {
        g_ParamError(L"출력 위치에 엑세스 권한없습니다!");
        return;
    }
    else if (ENOENT == OutR)
    {
        g_ParamError(L"출력 경로를 찾을 수 없습니다!");
        return;
    }

    vector<TypeParser*> vecParser;

    // 입력 파일 수집.
    for (int i(4) ; argc > i ; ++i)
    {
        wstring PacketFilePath(argv[i]);

        TypeParser* pParser(new TypeParser());
        if (false == pParser->Parse(PacketFilePath.c_str()))
        {
            PRINT(L"패킷 파싱 실패! 파일:" << PacketFilePath << endl);
            SAFE_DELETE(pParser);
            SAFE_DELETE_VEC(vecParser);
            return;
        }

        // 파싱된 데이터 수집.
        vecParser.push_back(pParser);
    }

    if (CPP_GEN_NAME == CodeType)
    {
        CppHandlerGenerator Gen;
        Gen.Initialize(OutPath, vecParser);
        if (false == Gen.Generate())
        {
            PRINT(L"C++ 핸들러 소스 코드 생성 실패 했습니다!" << endl);
        }
    }
    else if(CSHARP_GEN_NAME == CodeType)
    {
        CSHandlerGenerator Gen;
        Gen.Initialize(OutPath, vecParser);
        if (false == Gen.Generate())
        {
            PRINT(L"C# 핸들러 소스 코드 생성 실패 했습니다!" << endl);
        }
    }

    // 컨테이너 해제.
    SAFE_DELETE_VEC(vecParser);
}
