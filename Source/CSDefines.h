#pragma once

#define OUT_EXT_CS                      L".cs"
#define OUT_HANDLER_FILE_NAME           L"ConnectorBase.cs"
#define OUT_HANDLER_FILE_PARTIAL_NAME   L"ConnectorBase.@P1@.cs"

#define CS_TY_I64                       L"long"
#define CS_TY_I32                       L"int"
#define CS_TY_I16                       L"short"
#define CS_TY_UI64                      L"ulong"
#define CS_TY_UI32                      L"uint"
#define CS_TY_UI16                      L"ushort"
#define CS_TY_I8                        L"byte"
#define CS_TY_f64                       L"double"
#define CS_TY_f32                       L"float"

#define CS_TY_STR                       L"string"
#define CS_TY_WSTR                      L"string"

#define CS_TY_LIST                      L"System.Collections.Generic.List"

#define CS_TY_I64_DEFAULT_VAL           L"0"
#define CS_TY_I32_DEFAULT_VAL           L"0"
#define CS_TY_I16_DEFAULT_VAL           L"0"
#define CS_TY_UI64_DEFAULT_VAL          L"0"
#define CS_TY_UI32_DEFAULT_VAL          L"0"
#define CS_TY_UI16_DEFAULT_VAL          L"0"
#define CS_TY_I8_DEFAULT_VAL            L"0"
#define CS_TY_f64_DEFAULT_VAL           L"0"
#define CS_TY_f32_DEFAULT_VAL           L"0f"
#define CS_TY_STR_DEFAULT_VAL           L"\"\""

#define CS_IDF_P1                       L"@P1@"
#define CS_IDF_P2                       L"@P2@"
#define CS_IDF_P3                       L"@P3@"

#define CS_IDF_FIRST_ENUM_DATA          L"#FIRST_ENUM_DATA#"
#define CS_IDF_DEFAULT_VALUE            L"#DEFAULT_VALUE#"
#define CS_IDF_ARRAY_CNT                L"#ARRAY_CNT#"
#define CS_IDF_CONTAINER_TYPE           L"#CONTAINER_TYPE#"

