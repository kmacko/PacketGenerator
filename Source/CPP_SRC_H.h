#pragma once

#include <string>
using namespace std;

//////////////////////////////////////////////////////////////////////
// 패킷 정의 헤더
const wstring CPP_Header(L"\
#pragma once\n\
//========================================================\n\
// !PACKET_NAME!.h\n\
// !YYYYMMDD! By PacketGenerator.exe 자동 생성된 코드.\n\
// Generated From !PACKET_FILE_NAME!\n\
//========================================================\n\
\n\
");

const wstring CPP_IncludeHeader(L"\
#include \"!INCLUDE_NAME!.h\"\n\
");

const wstring CPP_Namespace(L"namespace !NAMESPACE!\n{\n");
const wstring CPP_NamespaceClose(L"}; // namespace !NAMESPACE!\n");


// 구조체.
const wstring CPP_StructStart(L"\
struct !STRUCT_NAME!\n\
{\n\
");

const wstring CPP_StructMember(L"    %STRUCT_VAR_TYPE% %STRUCT_VAR_NAME%;\n");

const wstring CPP_StructMemberDeclearContainerFormat(L"@P1@<@P2@>");
const wstring CPP_MemberNameDeclearArrayFormat(L"@P1@[@P2@]");

// @P1@:구조체이름
const wstring CPP_MessageType(L"\
\n\
\n\
    inline SHORT TYPE() { return (USHORT)k@P1@; }\n\
");

const wstring CPP_StructConstructor1(L"\
    !STRUCT_NAME!& operator=(!STRUCT_NAME!& Ref);\n\
\n\
    !STRUCT_NAME!();\n\
");
const wstring CPP_StructConstructor2(L"\
    !STRUCT_NAME!(%STRUCT_CONSTRUCT_MEMBER_LIST%);\n\
");
const wstring CPP_StructConstructor3(L"\
    !STRUCT_NAME!(const HNetPacket& Packet);\n\
\n\
    void SetData(const HNetPacket& Packet);\n\
    void SetPacket(HNET::NewPacket& pPacket);\n\
}; // struct !STRUCT_NAME!\n\
");

const wstring CPP_StructConstructMemberParamListRefFormat(L"@P1@@P2@& @P3@_");
const wstring CPP_StructConstructMemberParamListValueFormat(L"@P1@@P2@ @P3@_");
const wstring CPP_StructConstructMemberParamListArrayFormat(L"@P1@@P2@* @P3@_");

// 열거자.
const wstring CPP_EnumStart(L"\
enum !ENUM_NAME!\n\
{\n\
");

const wstring CPP_EnumDeclearFormat(L"    %s,\n");
const wstring CPP_EnumDeclearFormatWithValue(L"    %s = %d,\n");

const wstring CPP_EnumEnd(L"\
};\n\
\n\
extern map<int, wstring> g_Map!ENUM_NAME!Str;\n\
");

//////////////////////////////////////////////////////////////////////
// 패킷 핸들러 헤더.
// 패킷들을 하나의 헤더에 모으는 헤더의 헤더(PacketHeaders.h). 헤더만 있음.
const wstring CPP_HandlPacketHeadersComment(L"\
#pragma once\n\
//========================================================\n\
// PacketHeaders.h\n\
// !YYYYMMDD! By PacketGenerator.exe 자동 생성된 코드.\n\
// 패킷 정의 결과 헤더들을 여기에 모음.\n\
// 이 헤더 Include 한번으로 모든 패킷정의가 포함되게 한다.\n\
//========================================================\n\
");

const wstring CPP_HandlIncludePacket(L"\
#include \"!PACKET_FILE!.h\"\n\
");

const wstring CPP_HandlNamespaceUse(L"\
using namespace !NAMESPACE!;\n\
");

const wstring CPP_MakeNameSpceFormat(L"!NAMESPACE_FULL_STR!!NAMESPACE!");

const wstring CPP_HandlHCommentClassOpen(L"\
#pragma once\n\
//========================================================\n\
// PacketHandler.h\n\
// !YYYYMMDD! By PacketGenerator.exe 자동 생성된 코드.\n\
// 자동생성된 패킷들을 제어 처리 하는 클래스의 헤더.\n\
//========================================================\n\
#include \"PacketHeaders.h\"\n\
\n\
class PacketHandler : public HNET::Acceptor\n\
{\n\
protected:\n\
    PacketHandler();\n\
    virtual ~PacketHandler();\n\
\n\
public:\n\
");

const wstring CPP_HandlPacketVirtualFunc(L"\
    virtual void OnRecv!PROTOCOL_NAME!(NetId netID, const HNetPacket& Packet)\
{ UnDeclearMessageReceived(#ENUM_DATA_NAME#); }\n\
");

const wstring CPP_HandlHClassClose(L"\
\n\
public :\n\
    void UnDeclearMessageReceived(MessageType Type);\n\
    void UnKnownMessageType(USHORT Type);\n\
\n\
    void OnMessage(NetId netId, const HNetPacket& Packet) override;\n\
    void OnEncrypt(USHORT type, char* buf, USHORT lenth) override;\n\
    void OnDecrypt(USHORT type, char* buf, USHORT lenth) override;\n\
}; // class PacketHandler.h\n\
\n\
");
