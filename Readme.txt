
VS2017 에서 빌드함.

콘솔 프로젝트.

실행시 패킷 정의 파일을 파라메터를 받고 파싱 코드를 생성하는 툴.

자동 생성 지원 언어. C#(Unity), C++

기반 네트워크 엔진. HNetwork (https://cafe.naver.com/makehnetwork)

툴자체는 매우 단순함. 패킷 처리 소스 코드를 문자열 선언 해두고
패킷정의 내용과 물려지는 패킷 구조체, 변수, 자료형들을 반복 치환 입력 해넣는다.


사용상 주의점.
아무데서나 중괄호 대괄호 쓰면 에러난다.
이스케이프 문자 사용하면 오작동 할 수 있다. \ <- 요런거.

패킷 정의 파일들을 폴더링 하지 않는 것을 원칙으로 작업했음.
Include 걸리면서 헤더등에 #include 코드가 섞이는데 핸들러 등에서 폴더 위치를
못찾거나 중복 발생 할듯.
또한 패킷 생성툴exe 파일도 패킷정의 파일과 같은 위치에 놓길 바람.


목적.
1. 프로젝트 구현중 패킷 정의 패턴을 기반으로 반복적인 작업을 자동화 한다.
2. 반복적인 작업중 누락, 오타, 순서바뀜 등의 실수로 인한
   에러 확인 및 디버깅시간을 최소화한다.




기능 설명.

- namespace
  입력받은 파일에 선언 시점 이후 코드는 해당 네임스페이스로 감싼다.
  선언은 하나씩만 받는다. a.b.c 이런식은 안되고 하나씩..
  '.' 들어가면 에러 처리. (네임스페이스 지옥을 보고 싶지 않으면 남발하지 말자)

- struct
  패킷 또는 정보 구조체. 실제로는 클래스로(C#) 선언 구현된다.
  이 이름들은 패킷의 멤버로도 선언 가능하다.
  class 선언은 안받는다.

- 자료형.
  선언한 struct 이름,
  enum            -- 열거자 종류. MessageType(USHORT)을 제외하고 short로 전송함.
  i64             -- longlong, 8바이트 정수.
  i32             -- int 4바이트 정수.
  i16             -- short 2바이트 정수.
  i8              -- byte
  ui64            -- unsigned longlong 8바이트 부호없는 자료형.
  ui32            -- unsigned int 4바이트 부호없는 정수.
  ui16            -- unsigned short 2바이트 부호없는 정수.
  f64             -- double 8바이트 실수.
  f32             -- float 4바이트 실수.
  f16             -- 미구현 float를 압축한 버전. 좌표를 많이 다루면 추가 할 생각.
  string          -- 멀티바이트 문자열 c#은 그냥 string, utf8 인코딩 지정함.
  wstring         -- WideChar 문자열 c#은 그냥 string, Unicode 인코딩 지정함.
  []              -- 각 변수의 배열 선언. 컨테이너와 다름.
  list<>          -- 가능한 자료형 모두 가능. c#:list, c++:vector

  !! 구조체 밖에서의 자료형 선언은 에러낸다.
  !! 컨테이너 배열은 지원 안함
  !! 구조체의 배열선언은 지원 안함. 컨테이너로 사용할것.


- 주석.
  '//' 이후 한줄은 모두 무시한다.
  '/*' <- 이후 '*/' 를 만날때 까지 무시한다.

  처음에 주석을 소스코드에 함께 옮겨주려고 했으나 작업 복잡도가 너무 올라가서
  GG 쳐버렸다.
  주석 기능은 추후에 여유 있을 때 업데이트 해야 할듯.


- include "??"
  packet 정의 안에 다른 패킷 정의파일을 포함 할 수 있다.
  include 된 정의에 있는 구조체에 접근하려면
  해당 파일이름(확장자제외).선언된이름 접근 가능하다.
  Ex) include "TT.packet" 접근:TT.구조체, 열거자 이름

  *** 주의.
  구조체 이름 중복 체크 안함.
  include 안에 다시 include가 호출되어 중복 포함되는 경우
  원하지 않는 결과를 낼 수 있음. 포함관계 잘 정리 할것.

- Encrypt (엔진 - 유니티간에 에러 발생. 미구현)
  struct 이름 뒤에 쓴다. (패킷에만 적용 가능)
  구조체 이름뒤에 이 문자열이 붙으면 해당 패킷은 암호화 한다.
  (대소 문자 구분 안함.)
  암호화 방법은 버퍼배열에 ^= 0x?? 형태로..

  패킷 정의 중 하나에 다음의 커맨드 문자열이 있어야한다.
  비트 연산에 적용할 값을 문자열로 정의한다. (_Common.packet 참조)
  EncryptBitCode 0x23

- enum MessageType
  패킷 정의 파일 파싱 할때는 단순 열거자 취급하지만 패킷 핸들러 생성시에는
  패킷의 메세지 종류로 취급한다.
  이 이름은 고정이고 없으면 핸들러 생성시 에러남.

  핸들러 생성 하면서 첫글자가 소문자 이면 한글자를 삭제하고 프로토콜 이름으로
  사용한다. <- 이 문자열은 패킷구조체 이름과 같아야 한다.
  Ex)  kProtocol -> "void OnRecvProtocol() { UnDeclearMessageRecv(kProtocol); }"
  Ex1) protocol -> "void OnRecvrotocol() { UnDeclearMessageRecv(protocol); }"
  Ex2) Protocol -> "void OnRecvProtocol() { UnDeclearMessageRecv(Protocol); }"

--------- 디버깅시 명령 인수들. 출력경로는 상황에 맞게.

- 패킷 소스 생성. -옵션 언어 패킷파일 출력위치
-gen cpp _Common.packet ../../DDackzi_Server/Src/Protocol
-gen cpp Account.packet ../../DDackzi_Server/Src/Protocol
-gen cs _Common.packet cs
-gen cs Account.packet cs

- 패킷 핸들러 소스 생성. -옵션 언어 출력위치 패킷파일1 패킷파일2 ...
-handler cpp ../../DDackzi_Server/Src/Protocol _Common.packet Account.packet
-handler cs cs _Common.packet Account.packet

